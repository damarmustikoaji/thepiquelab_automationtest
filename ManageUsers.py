import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.manageusers.manageusers import InviteUser
from testscripts.manageusers.manageusers import DeleteUser
from testscripts.manageusers.manageusers import EditUser

import random
from random import randint
from testscripts.randomstring.randomstring import randomword

LOGIN = None
Fullname_ = None
nama = randomword(4)
FullnameMember = random.choice(["Mr. ", "Mrs. ", "Miss "])+nama.upper()
EmailMember = nama+"@mailinator.com"
HakAksesMember = random.choice(["User", "Admin"])
AvatarMember = ""#"/Users/wonderlabs/Desktop/damar/piquelab/image/"+random.choice(["ava1.png", "ava2.png", "ava3.png"])

class ManageUsers(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        #assert "Login - Piquelabs" in self.driver.title
        self.assertEqual(self.driver.title,"Login - Tutoract")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_b_manage_users_inviteuser(self):
        global LOGIN, Fullname_
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/users/")
            driver, logging, status = InviteUser(self.driver, FullnameMember, EmailMember, HakAksesMember, AvatarMember)
            print logging
            print status
            Fullname_ = status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_c_manage_users_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/users/")
            driver, logging, status = EditUser(self.driver, FullnameMember, EmailMember, HakAksesMember, AvatarMember)
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_d_manage_users_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/users/")
            driver, logging, status = DeleteUser(self.driver, FullnameMember+"Edited")
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_x_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        ManageUsers.BROWSER = sys.argv.pop()
        ManageUsers.SERVER = sys.argv.pop()
        if "http" not in ManageUsers.SERVER:
            ManageUsers.SERVER = "http://"+ManageUsers.SERVER
        ManageUsers.PASSWORD = sys.argv.pop()
        ManageUsers.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(ManageUsers)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
