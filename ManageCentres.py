import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.randomstring.randomstring import randomword
from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.managecentres.managecentres import CreateCentre
from testscripts.managecentres.managecentres import DeleteCentre
from testscripts.managecentres.managecentres import EditCentre

import random
from random import randint
from testscripts.randomstring.randomstring import randomword

LOGIN = None
NameCentre_ = None
nama = randomword(4)
NameCentre = random.choice(["Kampus ", "Sekolah ", "Bimbingan ", "Yayasan "])+nama.upper()
CentreAddress = "Jalan "+nama+" No. "+str(randint(1, 100))+" Yogyakarta, Indonesia"
AdminName = random.choice(["Mr. ", "Mrs. ", "Miss "])+nama.capitalize()
AdminEmail = nama+"@mailinator.com"

class ManageCentres(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        #assert "Login - Piquelabs" in self.driver.title
        self.assertEqual(self.driver.title,"Login - Tutoract")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_b_manage_centres_CreateCentre(self):
        global LOGIN, NameCentre_
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/centres/")
            driver, logging, status = CreateCentre(self.driver, NameCentre, CentreAddress, AdminName, AdminEmail)
            print logging
            print status
            NameCentre_ = status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_c_manage_centres_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/centres/")
            driver, logging, status = EditCentre(self.driver, NameCentre, NameCentre+"Edited", CentreAddress+"Edited")
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def _d_manage_centres_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/centres/")
            driver, logging, status = DeleteCentre(self.driver, NameCentre+"Edited")
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_x_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        ManageCentres.BROWSER = sys.argv.pop()
        ManageCentres.SERVER = sys.argv.pop()
        if "http" not in ManageCentres.SERVER:
            ManageCentres.SERVER = "http://"+ManageCentres.SERVER
        ManageCentres.PASSWORD = sys.argv.pop()
        ManageCentres.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(ManageCentres)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
