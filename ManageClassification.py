import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.randomstring.randomstring import randomword
from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.manageclassification.manageclassification import NewClassification
from testscripts.manageclassification.manageclassification import DeleteClassification
from testscripts.manageclassification.manageclassification import EditClassification

import random
from random import randint

LOGIN = None
NameClassification_ = None
TypeClass = "MCQs"#random.choice(["MCQs", "OEQs"])
NameClassification = "Classific Test"#random.choice(["Physics", "Biology", "Bahasa", "Maths", "Penjaskes"])
DescritionClassification = random.choice(["Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labor.", "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum."])
LevelClassification = "Primary 3"#random.choice(["Primary "+str(randint(3, 6)), "NA"])
ParentClassification = ""

class ManageClassification(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        #assert "Login - Piquelabs" in self.driver.title
        self.assertEqual(self.driver.title,"Login - Tutoract")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_b_manage_Classification_NewClassification(self):
        global LOGIN, NameCentre_, TypeClass_
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/classifications/")
            driver, logging, status = NewClassification(self.driver, TypeClass, NameClassification, DescritionClassification, LevelClassification, ParentClassification)
            print logging
            print status
            NameCentre_ = status
            TypeClass_ = TypeClass
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_c_manage_Classification_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/classifications/")
            driver, logging, status = EditClassification(self.driver, TypeClass, NameClassification)
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_d_manage_Classification_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/classifications/")
            driver, logging, status = DeleteClassification(self.driver, TypeClass, NameClassification+"Edited")#NameClassification+"Edited")
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_x_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        ManageClassification.BROWSER = sys.argv.pop()
        ManageClassification.SERVER = sys.argv.pop()
        if "http" not in ManageClassification.SERVER:
            ManageClassification.SERVER = "http://"+ManageClassification.SERVER
        ManageClassification.PASSWORD = sys.argv.pop()
        ManageClassification.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(ManageClassification)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
