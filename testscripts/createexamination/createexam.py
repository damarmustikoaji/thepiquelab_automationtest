import time
from selenium import webdriver
from random import randint
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

logging = None
status = None

def CreateExam(driver, School, Year, Level, Examination, MCQs, OEQs):
    driver = driver
    logging = None
    status = "FAIL"
    QuesMCQ = randint(1, 2) #format answer template
    QuestionMCQ = (["Which of the following substance(s) pass(es) through the stomata of the leaves?\n\nA: Oxygen\nB: water vapour\nC: carbon dioxide", "A only", "B only", "A and C only", "A, B and C"])
    QuestionOEQ = (["The statements below describes the different stages in the process of fertilization.\nA: The nucleus of the sperm fuses with the nucleus of the egg.\nB: Many sperms try to penetrate the egg.\nC: The fertilised egg divides.\nD: One sperm enters the egg successfully.", "Arrange the above stages of fertilization in the correct order in the boxes below.", "B"])
    try:
        card__body = driver.find_element_by_xpath('//div[@class="card__body"]')
        card__body.find_element_by_xpath('//div[@class="selectric"]').click()
        time.sleep(1)
        card__body.find_element_by_xpath('//li[contains(text(), "'+School+'")]').click()
        time.sleep(1)
        #driver.find_element_by_xpath("//select[@id='examination_paper_year']/option[text()='"+str(Year)+"']").click()
        driver.find_element_by_xpath('//label[contains(text(), "'+Level+'")]').click()
        driver.find_element_by_xpath('//label[contains(text(), "'+Examination+'")]').click()
        driver.find_element_by_xpath('//input[@id="examination_paper_mcq"]').send_keys(MCQs)
        driver.find_element_by_xpath('//input[@id="examination_paper_oeq"]').send_keys(OEQs)
        driver.find_element_by_xpath('//button[@type="submit" and @class="btn btn-default btn--blue margin-top-3"]').click()
        try:
            TempTipe = 1
            AnswerFormat = 1
            KeyMCQ = randint(1, 4)
            tab = driver.find_element_by_xpath('//ul[@class="nav nav-tabs" and @id="Tabs"]')
            try:
                tab.find_element_by_xpath('//a[@href="#multipleChoice"]').click()
                number_mcq = driver.find_elements_by_xpath('//ul[@id="examination-number-mcq"]/li/p')
                for value in number_mcq:
                    value.click()
                    driver, loggingMCQ, statusMCQ = Multiple(driver, TempTipe, AnswerFormat, QuestionMCQ, KeyMCQ, Level, Examination)
                    bradcrumb = driver.find_element_by_xpath('//ol[@class="breadcrumb"]')
                    hover = ActionChains(driver).move_to_element(bradcrumb)
                    hover.perform()
            except Exception as e:
                raise
            try:
                tab.find_element_by_xpath('//a[@href="#openEndedQuestion"]').click()
                time.sleep(2)
                number_oeq = driver.find_elements_by_xpath('//ul[@class="list-unstyled text-center" and @id="examination-number-oeq"]/li/p')
                for value in number_oeq:
                    value.click()
                    driver, loggingOEQ, statusOEQ = OpenEnded(driver, TempTipe, QuestionOEQ, Level, Examination)
                    bradcrumb = driver.find_element_by_xpath('//ol[@class="breadcrumb"]')
                    hover = ActionChains(driver).move_to_element(bradcrumb)
                    hover.perform()
                    time.sleep(2)
            except Exception as e:
                raise
            #statusMCQ == "PASS" and statusOEQ == "PASS":
            try:
                sticky = WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.ID, 'sticky')))
                sticky.find_element_by_xpath('//a[@class="btn btn-default btn-worksheets btn--full" and contains(text(), "SUBMIT")]').click()
                time.sleep(5)
                try:
                    alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                    logging = alert.text
                    if "Successfully" in logging:
                        status = "PASS"
                    else:
                        status = "FAIL"
                except Exception as e:
                    raise
            except Exception as e:
                raise
            #else:
            #    logging = loggingMCQ + " " + loggingOEQ
            #    status = "FAIL"
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def Multiple(driver, TempTipe, Format, Question, Key, Level, Examination):
    driver = driver
    logging = None
    status = "FAIL"
    time.sleep(2)
    Number = 1
    pick = driver.find_element_by_xpath('//form[@name="examination_mcq" and @id="submit-question-mcq"]')
    count = 0
    for template in pick.find_elements_by_xpath('//div[@class="block-grid pointer template-selected"]'):
        if template.is_displayed():
            count = count + 1
    if TempTipe <= count:
        try:
            no = 1
            for template in pick.find_elements_by_xpath('//div[@class="block-grid pointer template-selected"]'):
                if template.is_displayed():
                    if no == TempTipe:
                        template.click()
                        edit = 0
                        time.sleep(2)
                        #if "NA" in Level or "NA" in Examination:
                        #    pass
                        #else:
                        #    pick.find_element_by_id('examination_mcq_number').send_keys(Number)
                        for editor in driver.find_elements_by_xpath('//div[@class="fr-element fr-view" and @contenteditable="true"]'):
                            if editor.is_displayed():
                                hover = ActionChains(driver).move_to_element(editor)
                                hover.perform()
                                editor.click()
                                editor.send_keys(Question[edit])
                                edit = edit + 1
                        answers__data = driver.find_element_by_xpath('//div[@class="answers__data"]')
                        answers__data.find_element_by_xpath('//label[@class="radio__item" and contains(text(), "'+str(Key)+'")]').click()
                        answers__data.find_element_by_xpath('//label[@class="radio__item" and contains(text(), "'+str(Key)+'")]').click()
                        for button in driver.find_elements_by_xpath('//button[@class="btn btn-default btn--blue" and contains(text(), "SAVE")]'):
                            if button.is_displayed():
                                button.click()
                                try:
                                    alert = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="col-xs-11 col-sm-3 alert alert-success animated fadeInDown"]')))#col-xs-11 col-sm-3 alert alert-danger animated fadeInDown
                                    logging = alert.text
                                    if "saved" in logging:
                                        status = "PASS"
                                    else:
                                        status = "FAIL"
                                except Exception as e:
                                    logging = "Fail Save"
                                    status = "FAIL"
                        break
                    no = no + 1
        except Exception as e:
            raise
    return driver, logging, status

def OpenEnded(driver, TempTipe, Question, Level, Examination):
    Number = 1
    driver = driver
    logging = None
    status = "FAIL"
    time.sleep(2)
    pick = driver.find_element_by_xpath('//form[@name="examination_oeq" and @id="submit-question-oeq"]')
    #pick.find_element_by_xpath('//a[@class="btn btn-default btn--dark"]').click()
    count = 0
    for template in pick.find_elements_by_xpath('//div[@class="block-grid pointer template-selected"]'):
        if template.is_displayed():
            count = count + 1
    if TempTipe <= count:
        try:
            no = 1
            for template in pick.find_elements_by_xpath('//div[@class="block-grid pointer template-selected"]'):
                if template.is_displayed():
                    if no == TempTipe:
                        template.click()
                        #if "NA" in Level or "NA" in Examination:
                        #    pass
                        #else:
                        #    pick.find_element_by_id('examination_oeq_number').send_keys(Number)
                        driver.find_element_by_xpath('//a[@class="btn btn--dark" and @id="add-sub-quetion-button"]').click()
                        edit = 0
                        time.sleep(2)
                        for editor in driver.find_elements_by_xpath('//div[@class="fr-element fr-view" and @contenteditable="true"]'):
                            if editor.is_displayed():
                                hover = ActionChains(driver).move_to_element(editor)
                                hover.perform()
                                editor.click()
                                editor.send_keys(Question[edit])
                                edit = edit + 1
                        for score in pick.find_elements_by_xpath('//input[@class="form-control score-data" and @name="score[]"]'):
                            if score.is_displayed():
                                score.send_keys(1)
                        for button in driver.find_elements_by_xpath('//button[@class="btn btn--blue button-submit-oeq" and contains(text(), "SAVE")]'):
                            if button.is_displayed():
                                button.click()
                                try:
                                    alert = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="col-xs-11 col-sm-3 alert alert-success animated fadeInDown"]')))#col-xs-11 col-sm-3 alert alert-danger animated fadeInDown
                                    logging = alert.text
                                    if "saved" in logging:
                                        status = "PASS"
                                    else:
                                        status = "FAIL"
                                except Exception as e:
                                    logging = "Fail Save"
                                    status = "FAIL"
                        break
                    no = no + 1
        except Exception as e:
            raise
    return driver, logging, status
