import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import WebDriverException

def FindQuestionDB(driver, Level, School, Year, Examination, QuestionType):
    logging = None
    status = "FAIL"
    driver = driver
    wait = WebDriverWait(driver, 10)
    try:
        time.sleep(2)
        driver.find_element_by_xpath('//button[@type="button" and @class="btn btn-default btn--blue-outline collapsed"]').click()
        try:
            modal = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'div.swal2-container.swal2-center.swal2-fade.swal2-shown')))
            logging = modal.text
        except Exception as e:
            try:
                driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Level+'")]').click()
                driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+QuestionType+'")]').click()
                driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Examination+'")]').click()
                driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Used")]').click()
                driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Unused")]').click()
                button = driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue" and contains(text(), "Search Questions")]')
                ActionChains(driver).move_to_element(button).perform()
                time.sleep(1)
                button.click()
                time.sleep(2)
                try:
                    modal = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'div.swal2-container.swal2-center.swal2-fade.swal2-shown')))
                    logging = modal.text
                    status = "FAIL"
                except Exception as e:
                    try:
                        result = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="col-lg-3"]')))
                        logging = result.text
                        status = "PASS"
                    except Exception as e:
                        raise
            except Exception as e:
                raise
    except Exception as e:
        raise
    return driver, logging, status
