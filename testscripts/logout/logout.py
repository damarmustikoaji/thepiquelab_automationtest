from selenium import webdriver

logging = None
status = None

def Logout(driver):
    driver = driver
    try:
        signin = driver.find_element_by_xpath('//div[@class="content__box"]')
        logging = signin.find_element_by_tag_name('h3').text
        status = "PASS"
    except Exception as e:
        user = driver.find_element_by_xpath('//a[@class="dropdown-toggle user"]')
        logging = user.text
        status = "FAIL"
    return driver, logging, status
