import time
from selenium import webdriver
from random import randint
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

logging = None
status = None

def xCreateWorksheet(driver, WorksheetName, School, Year, Examination, Level):
    driver = driver
    logging = None
    status = "FAIL"
    try:
        worksheetname = driver.find_element_by_id('worksheet_name')
        worksheetname.click()
        worksheetname.send_keys(WorksheetName)
        driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue margin-top-1" and contains(text(), "Generate Worksheet")]').click()
        try:
            #selectMCQ
            form = driver.find_element_by_xpath('//form[@name="worksheet_mcq" and @id="submit-examination-paper"]')
            button = form.find_element_by_xpath('//button[@class="btn btn-default btn--pique-red" and contains(text(), "Find Question")]')
            #form.find_element_by_xpath("//select[@id='worksheet_mcq_mcqYear']/option[text()='"+str(Year)+"']").click()
            driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Level+'")]').click()
            driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Examination+'")]').click()
            driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Used")]').click()
            driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Unused")]').click()
            time.sleep(2)
            logging = button.text
            button.click()
            time.sleep(2)
            try:
                data = driver.find_element_by_id('data-question')
                logging = data.find_element_by_xpath('//div[@class="col-md-8 margin-top-1"]').text
            except Exception as e:
                try:
                    data = driver.find_element_by_xpath('//ul[@class="list-group question-list list-worksheet collapse in" and @id="multipleChoiceList"]')
                    logging = data.text
                    data_code = data.find_elements_by_xpath('//li[@class="list-group-item"]')
                    jumlah = randint(1,2)
                    no = 0
                    for code in data_code:
                        if no < jumlah:
                            code.find_element_by_tag_name('button').click()
                            time.sleep(2)
                        no = no + 1
                    status = "PASS"
                except Exception as e:
                    raise
            sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
            nextStep = sticky.find_element_by_xpath('//a[contains(text(), "NEXT")]')
            nextStep.click()
        except Exception as e:
            raise
        try:
            #selectOEQ
            form = driver.find_element_by_xpath('//form[@name="worksheet_oeq" and @id="submit-examination-paper"]')
            button = form.find_element_by_xpath('//button[@class="btn btn-default btn--pique-red" and contains(text(), "Find Question")]')
            #form.find_element_by_xpath("//select[@id='worksheet_mcq_mcqYear']/option[text()='"+str(Year)+"']").click()
            driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Examination+'")]').click()
            driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Level+'")]').click()
            driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Used")]').click()
            driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Unused")]').click()
            logging = button.text
            button.click()
            time.sleep(2)
            try:
                data = driver.find_element_by_id('data-question')
                logging = data.find_element_by_xpath('//div[@class="col-md-8 margin-top-1"]').text
            except Exception as e:
                try:
                    data = driver.find_element_by_xpath('//ul[@class="list-group question-list list-worksheet collapse in" and @id="openEndedList"]')
                    logging = data.text
                    data_code = data.find_elements_by_xpath('//li[@class="list-group-item"]')
                    jumlah = randint(1,2)
                    no = 0
                    for code in data_code:
                        if no < jumlah:
                            code.find_element_by_tag_name('button').click()
                            time.sleep(2)
                        no = no + 1
                    status = "PASS"
                except Exception as e:
                    raise
            sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
            nextStep = sticky.find_element_by_xpath('//a[contains(text(), "NEXT")]')
            nextStep.click()
        except Exception as e:
            raise
        try:
            #OrderQuestion
            data = driver.find_element_by_xpath('//ul[@class="list-group question-list list-worksheet collapse in list-worksheet--section ui-sortable" and @id="multipleChoiceList"]')
            logging = data.text
            data_code = data.find_elements_by_xpath('//li[@class="list-group-item ui-sortable-handle"]')
            jumlah = len(data_code)
            logging = jumlah
            no = 1
            for code in data_code:
                if no == 1:
                    code.find_element_by_xpath('//div[@class="link pointer" and contains(text(), "add topic")]').click()
                    modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal-wide swal2-show"]')))
                    modal.find_element_by_xpath('//input[@class="swal2-input"]').send_keys("Topic Section Ya")
                    modal.find_element_by_xpath('//button[@class="swal2-confirm swal2-styled"]').click()
                    time.sleep(2)
                    break
                no = no + 1
            sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
            nextStep = sticky.find_element_by_xpath('//a[contains(text(), "NEXT")]')
            nextStep.click()
        except Exception as e:
            raise
        try:
            #SelectCover
            form = driver.find_element_by_xpath('//form[@name="worksheet_cover" and @id="submit-cover-worksheet"]')
            cover = form.find_elements_by_xpath('//div[@class="block-grid pointer cover-selected"]')
            jumlah_cover = len(cover)
            pilih = randint(1, jumlah_cover)
            no = 0
            for cov in cover:
                if no == pilih:
                    cov.find_element_by_xpath('//input[@type="radio" and @name="worksheet_cover[styling]"]').click()
                    time.sleep(2)
                    cov.find_element_by_xpath('//input[@type="radio" and @name="worksheet_cover[styling]"]').click()
                    break
                no = no + 1
            sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
            nextStep = sticky.find_element_by_xpath('//button[@class="btn btn-default btn-worksheets-save btn--full" and contains(text(), "NEXT")]')
            nextStep.click()
            time.sleep(5)
        except Exception as e:
            raise
        try:
            #SelectFooter
            #form = driver.find_element_by_xpath('//form[@name="worksheet_footer" and @id="submit-footer-worksheet"]')
            form = driver.find_element_by_xpath('//div[@class="flex-container wrap margin-top-2"]')
            footer = form.find_elements_by_xpath('//div[@class="style-footer"]')
            jumlah_footer = len(footer)
            pilih = randint(1, jumlah_footer)
            no = 0
            time.sleep(2)
            for fot in footer:
                if no == pilih:
                    fot.find_element_by_xpath('//input[@type="radio" and @name="worksheet_footer[footer]"]').click()
                    time.sleep(2)
                    fot.find_element_by_xpath('//input[@type="radio" and @name="worksheet_footer[footer]"]').click()
                    break
                no = no + 1
            sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
            nextStep = sticky.find_element_by_xpath('//button[@class="btn btn-default btn-worksheets-save btn--full" and contains(text(), "NEXT")]')
            nextStep.click()
        except Exception as e:
            raise
        try:
            #Preview
            sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
            nextStep = sticky.find_element_by_xpath('//button[@class="btn btn-default btn-worksheets-save btn--full" and contains(text(), "NEXT")]')
            nextStep.click()
            try:
                modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
                modal.find_element_by_xpath('//button[@class="swal2-cancel swal2-styled"]').click()
            except Exception as e:
                raise
        except Exception as e:
            raise
        try:
            #SelectFolder
            time.sleep(5)
            selectfolder = driver.find_elements_by_xpath('//tr[@class="parent folder-select leaf expanded"]')
            jumlah_folder = len(selectfolder)
            #print jumlah_folder
            no = 1
            for folder in selectfolder:
                if no == 1:
                    folder.click()
                    time.sleep(5)
                    break
                no = no + 1
            sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
            nextStep = sticky.find_element_by_xpath('//button[@class="btn btn-default btn-worksheets-save btn--full" and contains(text(), "NEXT")]')
            nextStep.click()
            time.sleep(5)
            try:
                alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                logging = alert.text
                if "Success" in logging:
                    status = "PASS"
                else:
                    status = "FAIL"
            except Exception as e:
                raise
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status


def CreateWorksheet(driver, WorksheetName, School, Year, Examination, Level):
    driver = driver
    logging = None
    status = "FAIL"
    try:
        worksheetname = driver.find_element_by_id('worksheet_name')
        worksheetname.click()
        worksheetname.send_keys(WorksheetName)
        logging = worksheetname.text
        driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue margin-top-1" and contains(text(), "Generate Worksheet")]').click()
        status = "PASS"
        if status == "PASS":
            driver, logging, status = GenerateMCQ(driver, School, Year, Examination, Level)
            if status == "PASS":
                driver, logging, status = GenerateOEQ(driver, School, Year, Examination, Level)
                if status == "PASS":
                    driver, logging, status = OrderQuestion(driver)
                    if status == "PASS":
                        driver, logging, status = SelectCover(driver)
                        if status == "PASS":
                            driver, logging, status = SelectFooter(driver)
                            if status == "PASS":
                                driver, logging, status = Preview(driver)
                                if status == "PASS":
                                    driver, logging, status = SelectFolder(driver)
                                else:
                                    status = "FAIL"
                                    logging = logging
                            else:
                                status = "FAIL"
                                logging = logging
                        else:
                            status = "FAIL"
                            logging = logging
                    else:
                        status = "FAIL"
                        logging = logging
                else:
                    status = "FAIL"
                    logging = logging
            else:
                status = "FAIL"
                logging = logging
        else:
            status = "FAIL"
            logging = logging
    except Exception as e:
        raise
    return driver, logging, status

def GenerateMCQ(driver, School, Year, Examination, Level):
    try:
        #selectMCQ
        form = driver.find_element_by_xpath('//form[@name="worksheet_mcq" and @id="submit-examination-paper"]')
        button = form.find_element_by_xpath('//button[@class="btn btn-default btn--pique-red" and contains(text(), "Find Question")]')
        #form.find_element_by_xpath("//select[@id='worksheet_mcq_mcqYear']/option[text()='"+str(Year)+"']").click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Level+'")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Examination+'")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Used")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Unused")]').click()
        time.sleep(2)
        logging = button.text
        button.click()
        time.sleep(2)
        try:
            data = driver.find_element_by_id('data-question')
            logging = data.find_element_by_xpath('//div[@class="col-md-8 margin-top-1"]').text
        except Exception as e:
            try:
                data = driver.find_element_by_xpath('//ul[@class="list-group question-list list-worksheet collapse in" and @id="multipleChoiceList"]')
                logging = data.text
                data_code = data.find_elements_by_xpath('//li[@class="list-group-item"]')
                jumlah = randint(1,2)
                no = 0
                for code in data_code:
                    if no < jumlah:
                        code.find_element_by_tag_name('button').click()
                        time.sleep(2)
                    no = no + 1
                status = "PASS"
            except Exception as e:
                raise
        sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
        nextStep = sticky.find_element_by_xpath('//a[contains(text(), "NEXT")]')
        nextStep.click()
    except Exception as e:
        raise
    return driver, logging, status

def GenerateOEQ(driver, School, Year, Examination, Level):
    try:
        form = driver.find_element_by_xpath('//form[@name="worksheet_oeq" and @id="submit-examination-paper"]')
        button = form.find_element_by_xpath('//button[@class="btn btn-default btn--pique-red" and contains(text(), "Find Question")]')
        #form.find_element_by_xpath("//select[@id='worksheet_mcq_mcqYear']/option[text()='"+str(Year)+"']").click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Examination+'")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Level+'")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Used")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Unused")]').click()
        logging = button.text
        button.click()
        time.sleep(2)
        try:
            data = driver.find_element_by_id('data-question')
            logging = data.find_element_by_xpath('//div[@class="col-md-8 margin-top-1"]').text
        except Exception as e:
            try:
                data = driver.find_element_by_xpath('//ul[@class="list-group question-list list-worksheet collapse in" and @id="openEndedList"]')
                logging = data.text
                data_code = data.find_elements_by_xpath('//li[@class="list-group-item"]')
                jumlah = randint(1,2)
                no = 0
                for code in data_code:
                    if no < jumlah:
                        code.find_element_by_tag_name('button').click()
                        time.sleep(2)
                    no = no + 1
                status = "PASS"
            except Exception as e:
                raise
        sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
        nextStep = sticky.find_element_by_xpath('//a[contains(text(), "NEXT")]')
        nextStep.click()
    except Exception as e:
        raise
    return driver, logging, status

def OrderQuestion(driver):
    try:
        #OrderQuestion
        time.sleep(5)
        data = driver.find_element_by_xpath('//ul[@class="list-group question-list list-worksheet collapse in list-worksheet--section ui-sortable" and @id="multipleChoiceList"]')
        logging = data.text
        data_code = data.find_elements_by_xpath('//li[@class="list-group-item ui-sortable-handle"]')
        jumlah = len(data_code)
        logging = jumlah
        no = 1
        for code in data_code:
            if no == 1:
                code.find_element_by_xpath('//div[@class="link pointer" and contains(text(), "add topic")]').click()
                modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal-wide swal2-show"]')))
                modal.find_element_by_xpath('//input[@class="swal2-input"]').send_keys("Topic Section Ya")
                modal.find_element_by_xpath('//button[@class="swal2-confirm swal2-styled"]').click()
                time.sleep(2)
                status = "PASS"
                break
            no = no + 1
        sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
        nextStep = sticky.find_element_by_xpath('//a[contains(text(), "NEXT")]')
        nextStep.click()
    except Exception as e:
        raise
    return driver, logging, status

def SelectCover(driver):
    try:
        #SelectCover
        noda_data = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, '//div[@id="row-nodata"]')))
        status = "FAIL"
        logging = noda_data.text
    except Exception as e:
        try:
            card = driver.find_elements_by_xpath('//div[@class="card card--bg-dark"]')
            logging = len(card)
            status = "PASS"
            card[0].click()
            time.sleep(2)
            sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
            nextStep = sticky.find_element_by_xpath('//button[@class="btn btn-default btn-worksheets-save btn--full" and contains(text(), "NEXT")]')
            nextStep.click()
        except Exception as e:
            raise
    return driver, logging, status

def SelectFooter(driver):
    try:
        #SelectCover
        noda_footer = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, '//div[@id="row-nodata"]')))
        status = "FAIL"
        logging = noda_footer.text
    except Exception as e:
        try:
            card = driver.find_elements_by_xpath('//div[@class="style-footer"]')
            logging = len(card)
            status = "PASS"
            card[0].find_element_by_xpath('//div[@class="radio radio-danger display-flex"]').click()
            time.sleep(2)
            sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
            nextStep = sticky.find_element_by_xpath('//button[@class="btn btn-default btn-worksheets-save btn--full" and contains(text(), "NEXT")]')
            nextStep.click()
        except Exception as e:
            raise
    return driver, logging, status

def Preview(driver):
    try:
        #Preview
        sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
        nextStep = sticky.find_element_by_xpath('//button[@class="btn btn-default btn-worksheets-save btn--full" and contains(text(), "NEXT")]')
        nextStep.click()
        try:
            modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
            logging = modal.text
            modal.find_element_by_xpath('//button[@class="swal2-cancel swal2-styled"]').click()
            status = "PASS"
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def SelectFolder(driver):
    try:
        #SelectFolder
        time.sleep(5)
        selectfolder = driver.find_elements_by_xpath('//tr[@class="parent folder-select leaf expanded"]')
        jumlah_folder = len(selectfolder)
        #print jumlah_folder
        no = 1
        for folder in selectfolder:
            if no == 1:
                folder.click()
                time.sleep(5)
                break
            no = no + 1
        sticky = driver.find_element_by_xpath('//div[@class="card" and @id="sticky"]')
        nextStep = sticky.find_element_by_xpath('//button[@class="btn btn-default btn-worksheets-save btn--full" and contains(text(), "NEXT")]')
        nextStep.click()
        time.sleep(5)
        try:
            alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
            logging = alert.text
            if "Success" in logging:
                status = "PASS"
            else:
                status = "FAIL"
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status
