import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import WebDriverException

logging = None
status = None

def CreateCentre(driver, NameCentre, CentreAddress, AdminName, AdminEmail):
    driver = driver
    button = driver.find_element_by_xpath('//button[@type="submit" and @class="btn btn-default btn--blue-outline"]')
    button.click()
    try:
        centre_name = driver.find_element_by_id("centre_name")
        centre_name.send_keys(NameCentre)
        centre_addrs = driver.find_element_by_id("centre_address")
        centre_addrs.send_keys(CentreAddress)
        admin_name = driver.find_element_by_id("centre_users___name___name")
        admin_name.send_keys(AdminName)
        admin_email = driver.find_element_by_id("centre_users___name___email")
        admin_email.send_keys(AdminEmail)
        try:
            button = driver.find_element_by_xpath('//button[@class="btn btn--blue"]')
            button.click()
            try:
                alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                logging = alert.text
                if "succeeded" in logging:
                    status = "PASS"
                else:
                    status = "FAIL"
            except Exception as e:
                raise
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def DeleteCentre(driver, NameCentre):
    driver = driver
    try:
        baris = driver.find_elements_by_tag_name('tr')
        for nama_centres in baris:
            temp = nama_centres
            if NameCentre in temp.text:
                try:
                    logging = temp.text
                    temp.find_element_by_link_text("Delete").click()
                    try:
                        jsalert= driver.switch_to.alert
                        jsalert.accept()
                        time.sleep(5)
                        try:
                            baris = driver.find_elements_by_tag_name('tr')
                            for nama_centres in baris:
                                if NameCentre in nama_centres.text:
                                    status = "FAIL"
                                else:
                                    status = "PASS"
                        except Exception as e:
                            raise
                    except Exception as e:
                        raise
                    break
                except Exception as e:
                    raise
            else:
                logging = "Not found: "+NameCentre
                status = "FAIL"
    except Exception as e:
        raise
    return driver, logging, status

def EditCentre(driver, NameCentre, edit_NameCentre, edit_CentreAddress):
    driver = driver
    try:
        baris = driver.find_elements_by_tag_name('tr')
        for namamember in baris:
            temp = namamember
            if NameCentre in temp.text:
                try:
                    logging = temp.text
                    temp.find_element_by_link_text("Edit").click()
                    try:
                        edit_NameCentre = edit_NameCentre
                        centre_name = driver.find_element_by_id("edit_centre_name")
                        centre_name.clear()
                        centre_name.send_keys(edit_NameCentre)
                        if centre_name.get_attribute('value') == edit_NameCentre:
                            driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue"]').click()
                            try:
                                alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                                logging = alert.text
                                if "succeeded" in logging:
                                    status = "PASS"
                                else:
                                    status = "FAIL"
                            except Exception as e:
                                raise
                    except Exception as e:
                        raise
                    break
                except Exception as e:
                    raise
            else:
                logging = "Not found: "+NameCentre
                status = "FAIL"
    except Exception as e:
        raise
    return driver, logging, status
