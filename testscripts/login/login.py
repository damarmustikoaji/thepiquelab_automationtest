import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

logging = None
status = None

def Login(driver, EMAIL, PASSWORD):
    driver = driver
    try:
        #email = driver.find_element_by_name("_username")
        email = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.NAME,"_username")))
        #password = driver.find_element_by_name("_password")
        password = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.NAME,"_password")))
        email.send_keys(EMAIL)
        password.send_keys(PASSWORD)
        #driver.save_screenshot('capture/login_pique.png')
        if email.get_attribute('value') == EMAIL and password.get_attribute('value') == PASSWORD:
            logging = email.get_attribute('value')+"/"+password.get_attribute('value')
            driver.find_element_by_xpath('//button[@type="submit" and @class="btn btn-default btn-block btn--pique"]').click()
            try:
                user = driver.find_element_by_xpath('//a[@class="dropdown-toggle user"]')
                logging = user.text
                status = "PASS"
            except Exception as e:
                alert = driver.find_element_by_xpath('//div[@class="alert alert-danger"]')
                logging = alert.text
                status = "FAIL"
            #driver.save_screenshot('capture/dashboard_pique.png')
        else:
            logging = email.get_attribute('value')+"/"+password.get_attribute('value')
        #time.sleep(5)
    except Exception as e:
        raise
    return driver, logging, status
