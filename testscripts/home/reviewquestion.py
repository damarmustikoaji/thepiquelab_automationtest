import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
import random
from random import randint

logging = None
status = None

def ReviewQuestion(driver, User):
    driver = driver
    review = random.choice(["Accept", "Reject"])
    try:
        baris = driver.find_elements_by_tag_name('tr')
        for submitted in baris:
            temp = submitted
            if User in temp.text:
                try:
                    logging = temp.text
                    temp.find_element_by_link_text("Show").click()
                    try:
                        count = len(driver.find_elements_by_xpath('//li[@class="list-group-item"]'))
                        no = 1
                        for code in driver.find_elements_by_xpath('//li[@class="list-group-item"]'):
                            if no == count:
                                logging = code.text
                                code.click()
                                try:
                                    preview = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="question-preview"]')))
                                    time.sleep(5)
                                    if review == "Accept":
                                        floatingbutton = driver.find_element_by_xpath('//div[@id="sticky"]')
                                        floatingbutton.find_element_by_xpath('//label[@for="acceptQuestion"]').click()
                                        try:
                                            modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
                                            logging = logging +" "+ modal.text
                                            modal.find_element_by_xpath('//button[@type="button" and @class="swal2-confirm swal2-styled"]').click()
                                            status = "PASS"
                                        except Exception as e:
                                            raise
                                    elif review == "Reject":
                                        floatingbutton = driver.find_element_by_xpath('//div[@id="sticky"]')
                                        floatingbutton.find_element_by_xpath('//label[@for="rejectQuestion"]').click()
                                        try:
                                            modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal-wide swal2-show"]')))
                                            modal.find_element_by_xpath('//textarea[@class="swal2-textarea"]').send_keys("Reject Ya")
                                            time.sleep(2)
                                            logging = logging +" "+ modal.text
                                            modal.find_element_by_xpath('//button[@type="button" and @class="swal2-confirm swal2-styled"]').click()
                                            status = "PASS"
                                        except Exception as e:
                                            raise
                                    else:
                                        logging = review
                                        status = "FAIL"
                                except Exception as e:
                                    raise
                            no = no + 1
                    except Exception as e:
                        raise
                    break
                except Exception as e:
                    raise
            else:
                logging = "Not found: "+User
                status = "FAIL"
    except Exception as e:
        raise
    return driver, logging, status
