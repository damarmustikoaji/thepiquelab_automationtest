import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import WebDriverException

logging = None
status = None

def InviteUser(driver, FullnameMember, EmailMember, HakAksesMember, AvatarMember):
    driver = driver
    #AvatarMember = "/Users/wonderlabs/Desktop/damar/piquelab/image/ava1.png"
    try:
        driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue-outline"]').click()
        try:
            fullname = driver.find_element_by_id("invite_name")
            fullname.send_keys(FullnameMember)
            designation = driver.find_element_by_id("invite_designation")
            designation.send_keys("Tester")
            email = driver.find_element_by_id("invite_email")
            email.send_keys(EmailMember)
            #driver.find_element_by_xpath('//input[@type="file" and @class="input-img"]').send_keys(AvatarMember)
            time.sleep(3)
            if HakAksesMember == "Admin":
                driver.find_element_by_id("invite_actor_0").click()
            elif HakAksesMember == "User":
                driver.find_element_by_id("invite_actor_1").click()
            try:
                footer = driver.find_element_by_xpath('//div[@class="divider margin-top-3 margin-bottom-3"]')
                ActionChains(driver).move_to_element(footer).perform()
                time.sleep(2)
                button = driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue"]')
                button.click()
                try:
                    alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                    logging = alert.text
                    if "succeeded" in logging:
                        status = "PASS"
                except Exception as e:
                    try:
                        alert = driver.find_element_by_xpath('//div[@class="alert alert-danger"]')
                        logging = alert.text
                        status = "FAIL"
                    except Exception as e:
                        raise
            except Exception as e:
                raise
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def DeleteUser(driver, FullnameMember):
    driver = driver
    try:
        baris = driver.find_elements_by_tag_name('tr')
        for namamember in baris:
            temp = namamember
            if FullnameMember in temp.text:
                try:
                    logging = temp.text
                    temp.find_element_by_link_text("Delete").click()
                    try:
                        modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
                        logging = modal.text
                        modal.find_element_by_xpath('//button[@type="button" and @class="swal2-confirm swal2-styled"]').click()
                        time.sleep(3)
                        try:
                            baris = driver.find_elements_by_tag_name('tr')
                            for namamember in baris:
                                if FullnameMember in namamember.text:
                                    status = "FAIL"
                                else:
                                    status = "PASS"
                        except Exception as e:
                            raise
                    except Exception as e:
                        raise
                    break
                except Exception as e:
                    raise
            else:
                logging = "Not found: "+FullnameMember
                status = "FAIL"
    except Exception as e:
        raise
    return driver, logging, status

def EditUser(driver, FullnameMember, EmailMember, HakAksesMember, AvatarMember):
    driver = driver
    try:
        baris = driver.find_elements_by_tag_name('tr')
        for namamember in baris:
            temp = namamember
            if FullnameMember in temp.text:
                try:
                    logging = temp.text
                    temp.find_element_by_link_text("Edit").click()
                    try:
                        #FullnameMember = FullnameMember+"Edited"
                        fullname = driver.find_element_by_id("user_edit_name")
                        #fullname.clear()
                        fullname.send_keys("Edited")
                        fullname.click()
                        time.sleep(2)
                        #if fullname.get_attribute('value') == FullnameMember:
                        button = driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue"]')#.click()
                        ActionChains(driver).move_to_element(button).perform()
                        time.sleep(2)
                        button.click()
                        time.sleep(2)
                        try:
                            alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                            logging = alert.text
                            if "succeeded" in logging:
                                status = "PASS"
                        except Exception as e:
                            try:
                                alert = driver.find_element_by_xpath('//div[@class="alert alert-danger"]')
                                logging = alert.text
                                status = "FAIL"
                            except Exception as e:
                                raise
                    except Exception as e:
                        raise
                    break
                except Exception as e:
                    raise
            else:
                logging = "Not found: "+FullnameMember
                status = "FAIL"
    except Exception as e:
        raise
    return driver, logging, status
