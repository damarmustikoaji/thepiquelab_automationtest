import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

def NewCover(driver, Name):
    logging = None
    status = None
    driver = driver
    no = 0
    try:
        print len(driver.find_elements_by_xpath('//div[@class="card card--bg-dark"]'))
        driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue-outline"]').click()
        try:
            driver.find_element_by_id('styling_name').send_keys(Name)
            driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue" and contains(text(), "SAVE")]').click()
            time.sleep(5)
            try:
                alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                logging = alert.text
                if "succeeded" in logging:
                    status = "PASS"
                else:
                    status = "FAIL"
            except Exception as e:
                raise
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def DeleteCover(driver):
    logging = None
    status = None
    driver = driver
    no = 1
    try:
        styleCover = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH, '//div[@class="tab-pane fade in active" and @id="styleCover"]')))
        cover = styleCover.find_elements_by_xpath('//div[@class="block-grid"]')
        before = len(cover)
        if before > 0:
            for pilih in cover:
                if no == 1:
                    hover = ActionChains(driver).move_to_element(pilih)
                    hover.perform()
                    pilih.find_element_by_tag_name('svg').click()
                    time.sleep(2)
                    try:
                        modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
                        logging = modal.text
                        modal.find_element_by_xpath('//button[@class="swal2-confirm swal2-styled"]').click()
                        time.sleep(3)
                        status = "PASS"
                    except Exception as e:
                        raise
                    break
                no = no + 1
        else:
            logging = "No Data"
            status = "FAIL"
    except Exception as e:
        raise
    return driver, logging, status

def EditCover(driver):
    logging = None
    status = None
    driver = driver
    no = 1
    try:
        styleCover = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH, '//div[@class="tab-pane fade in active" and @id="styleCover"]')))
        cover = styleCover.find_elements_by_xpath('//div[@class="block-grid"]')
        before = len(cover)
        if before > 0:
            for pilih in cover:
                if no == 1:
                    hover = ActionChains(driver).move_to_element(pilih)
                    hover.perform()
                    pilih.find_element_by_xpath('//div[@class="card__footer text-center"]/a[contains(text(), "Edit")]').click()
                    try:
                        field = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.ID, "styling_name")))
                        field.send_keys("Edited")
                        time.sleep(2)
                        button = driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue"]')
                        hover = ActionChains(driver).move_to_element(button)
                        hover.perform()
                        button.click()
                        time.sleep(5)
                        try:
                            styleCover = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH, '//div[@class="tab-pane fade in active" and @id="styleCover"]')))
                            cover = styleCover.find_elements_by_xpath('//div[@class="block-grid"]')
                            status = "PASS"
                        except Exception as e:
                            raise
                    except Exception as e:
                        raise
                    break
                no = no + 1
        else:
            logging = "No Data"
            status = "FAIL"
    except Exception as e:
        raise
    return driver, logging, status

def NewFooter(driver, Text, Img):
    logging = None
    status = None
    driver = driver
    Img = '/Users/wonderlabs/damar/Belajar/thepiquelab_automationtest/file/Footer.png'
    no = 0
    try:
        Tab_footer = driver.find_element_by_xpath('//a[@href="#styleFooter" and contains(text(), "Footer")]')
        Tab_footer.click()
        time.sleep(2)
        try:
            create = driver.find_element_by_xpath('//a[@class="btn btn-default btn--blue-outline" and contains(text(), "CREATE FOOTER")]')
            create.click()
            try:
                teks = driver.find_element_by_xpath('//input[@id="footer_content" and @name="footer[content]"]')
                image = driver.find_element_by_xpath('//input[@type="file" and @class="form-control pad-left-0 input-img"]')
                teks.send_keys(Text)
                driver.find_element_by_xpath('//input[@id="footer_contentPosition_0"]').click()
                try:
                    image.send_keys(Img)
                    time.sleep(5)
                    driver.find_element_by_xpath('//button[@class="btn btn--blue" and contains(text(), "SAVE")]').click()
                    try:
                        alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                        logging = alert.text
                        if "succeeded" in logging:
                            status = "PASS"
                        else:
                            status = "FAIL"
                    except Exception as e:
                        raise
                except Exception as e:
                    status = "FAIL"
                    logging = "Fail send_keys path file"
            except Exception as e:
                raise
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def DeleteFooter(driver, Text):
    logging = None
    status = None
    driver = driver
    no = 0
    try:
        tab = driver.find_element_by_xpath('//ul[@class="nav nav-tabs" and @id="Tabs"]')
        tab.find_element_by_xpath('//a[@href="#styleFooter" and contains(text(), "Footer")]').click()
        time.sleep(2)
        try:
            data = driver.find_elements_by_xpath('//div[@class="style-footer"]')
            no = 0
            for footer in data:
                #if Text in footer.text:
                if no == 0:
                    pass
                    footer.find_element_by_xpath('//span[@onclick="return confirmDeleteFooter(this);"]').click()
                    time.sleep(2)
                    try:
                        modal = WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
                        logging = modal.text
                        modal.find_element_by_xpath('//button[@class="swal2-confirm swal2-styled"]').click()
                        time.sleep(2)
                        status = "PASS"
                    except Exception as e:
                        raise
                    break
                no = no + 1
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status
