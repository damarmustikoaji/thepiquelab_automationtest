import time
from selenium import webdriver
from random import randint
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

logging = None
status = None


def CreateQuestionMCQ(driver, Type, TempTipe, School, Year, Level, Examination, Number):
    driver = driver
    logging = None
    status = "FAIL"
    #acak = randint(1, 2)
    format_jawaban = randint(1, 5) #format answer template
    if format_jawaban == 1:
        Question = "Calvin stretched the mouth of a balloon over the mouth of a thin glass bottle. He then placed the glass bottle into a container of hot water. After a short while, he noticed that the balloon was inflated. What can Jim conclude from the above results?"
        Answer = (["The number of batteries does not affect of the strength of electromagnet."
                ,"The number of turns of coil of wire around the iron nail does not affect the strength of the electromagnet."
                ,"The strength of the electromagnet depends only on the number of coils of wire around the iron nail."
                ,"The strength of the electromagnet depends on both the number of bateries and the number of coils of wire around the iron nail."])
    elif format_jawaban == 2:
        Question = "Which of the following substance(s) pass(es) through the stomata of the leaves?\n\nA: Oxygen\nB: water vapour\nC: carbon dioxide\n"
        Answer = (["A only", "B only", "A and C only", "A, B and C"])
    elif format_jawaban == 3:
        Question = "The diagram below shows the cross-section of a flower.\nWhich part is not matched to its function?"
        Answer = (["A", "Produces pollen grains", "B", "Holds up the anther", "C", "Contains the egg cells", "D", "Develops into a fruit"])
        Header = (["Part", "Function"])
    elif format_jawaban == 4:
        Question = "Allan carries out the following procedures for an experiment. \nA: He weighed the soccer ball at first. \nB: He then pumped more air into the ball and observed that the size of the ball did not change. \nC: He weighed it again and recorded the mass. \nWhich of the following is most likely the results and conclusion of his experiment?"
        Answer = (["500g", "480g", "Air takes up space and has no mass.", "500g","500g","Air has no mass.","500g","520g","Air has mass and can be compressed.","500g","600g","Air has a definite volume."])
        Header = (["Mass of ball at the start of the experiment", "Mass of ball at the end of the experiment", "Conclusion"])
    elif format_jawaban == 5:
        Question = "Oliver released a metal ball from the top of slope X. The ball rolled down the slope, along the floor, up and down ramp Y and was blocked by tower Z, before it came to a stop.\nBased on the experiment, Oliver made the following statements. Which one of the following statement(s) is/are correct?\n\nA: The ball had the most potential energy at the top of slope X.\nB: The ball stopped at tower Z as it had the most kinetic energy.\nC: The potential energy of the ball increased as it rolled down ramp Y.\nD: The kinetic energy of the ball was the highest at the foot of slope X."
        Answer = (["A only", "B only", "C only", "D only", "A and D only", "A and D only", "A and D only", "A and D only", "B and C only", "B and C only", "B and C only", "B and C only", "A, B, C and D", "A, B, C and D", "A, B, C and D", "A, B, C and D"])
        Header = (["Answer 1", "Answer 2", "Answer 3", "Answer 4"])
    Key = randint(1, 4)
    no = 0
    try:
        tab = driver.find_element_by_xpath('//li[@role="presentation"]')
        if Type == "MCQs":
            tab.find_element_by_xpath('//a[@href="#multipleChoice"]').click()
        time.sleep(2)
        try:
            pick = driver.find_element_by_xpath('//div[@class="flex-container wrap"]')
            count = len(pick.find_elements_by_xpath('//div[@class="block-grid pointer template-selected"]'))
            try:
                no_template = pick.find_element_by_xpath('//div[@class="row" and @id="row-nodata"]')
                if no_template.is_displayed():
                    logging = no_template.text
                    status = "FAIL"
            except Exception as e:
                try:
                    if count > 0:
                        if TempTipe <= count:
                            TempTipe = TempTipe - 1
                            pick.find_elements_by_xpath('//div[@class="card card--bg-dark"]')[TempTipe].click()
                            #driver.find_element_by_xpath("//select[@id='question_school']/option[text()='"+School+"']").click()
                            card__body = driver.find_element_by_xpath('//div[@class="card__body"]')
                            card__body.find_element_by_xpath('//div[@class="selectric"]').click()
                            time.sleep(1)
                            card__body.find_element_by_xpath('//li[contains(text(), "'+School+'")]').click()
                            time.sleep(1)
                            #driver.find_element_by_xpath("//select[@id='question_year']/option[text()='"+str(Year)+"']").click()
                            card__body.find_element_by_xpath('//label[contains(text(), "'+Level+'")]').click()
                            time.sleep(1)
                            card__body.find_element_by_xpath('//label[contains(text(), "'+Examination+'")]').click()
                            time.sleep(1)
                            if "NA" in Level or "NA" in Examination:
                                pass
                            else:
                                card__body.find_element_by_id('question_number').send_keys(Number)
                            driver.find_element_by_xpath('//div[@class="fr-element fr-view"]').send_keys(Question)
                            answer_format = driver.find_element_by_xpath('//div[@class="format-question flex-container wrap"]')
                            logging = len(answer_format.find_elements_by_xpath('//div[@class="block-grid block-grid--transparent pointer template-choice"]'))
                            if format_jawaban > 0 and format_jawaban <= logging:
                                try:
                                    no = 1
                                    for answer_temp in answer_format.find_elements_by_xpath('//div[@class="block-grid block-grid--transparent pointer template-choice"]'):
                                        if no == format_jawaban:
                                            answer_temp.click()
                                            answer_temp.click()
                                            time.sleep(1)
                                            answers_ = driver.find_element_by_xpath('//div[@class="answers__body" and @id="data-option-mcq"]')
                                            if format_jawaban > 2:
                                                head = 0
                                                for headercol in answers_.find_elements_by_xpath('//input[@class="form-control pad-left-2" and @name="header[]"]'):
                                                    if headercol.is_displayed():
                                                        headercol.send_keys(Header[head])
                                                        head = head + 1
                                            print len(answers_.find_elements_by_xpath('//div[@class="fr-element fr-view"]'))
                                            ed = 1
                                            an = 0
                                            for editor in answers_.find_elements_by_xpath('//div[@class="fr-element fr-view"]'):
                                                if editor.is_displayed():
                                                    if ed > 1:
                                                        editor.click()
                                                        editor.send_keys(Answer[an])
                                                        time.sleep(1)
                                                        an = an + 1
                                                    ed = ed + 1
                                            break
                                        no = no + 1
                                except Exception as e:
                                    raise
                            answers__data = driver.find_element_by_xpath('//div[@class="answers__data"]')
                            answers__data.find_element_by_xpath('//label[@class="radio__item" and contains(text(), "'+str(Key)+'")]').click()
                            time.sleep(1)
                            answers__data.find_element_by_xpath('//label[@class="radio__item" and contains(text(), "'+str(Key)+'")]').click()
                            time.sleep(1)
                            driver.find_element_by_xpath('//button[@type="submit" and @id="btn-submit-mcq"]').click()
                            time.sleep(5)
                            try:
                                alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                                logging = alert.text
                                if "Successfully" in logging:
                                    status = "PASS"
                                else:
                                    status = "FAIL"
                            except Exception as e:
                                raise
                        else:
                            logging = "No Found Template"
                    else:
                        logging = "No Template Data"
                except Exception as e:
                    raise
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def CreateQuestionOEQ(driver, Type, TempTipe, School, Year, Level, Examination, Number):
    driver = driver
    logging = None
    status = "FAIL"
    acak = randint(1, 3)
    if acak == 1:
        Question = (["The statements below describes the different stages in the process of fertilization.\nA: The nucleus of the sperm fuses with the nucleus of the egg.\nB: Many sperms try to penetrate the egg.\nC: The fertilised egg divides.\nD: One sperm enters the egg successfully.", "Arrange the above stages of fertilization in the correct order in the boxes below.", "B"])
    elif acak == 2:
        Question = (["Jonathan set up the circuit below.", "When the switch in the circuit is closed, the buzzer produces a sound. Explain how this happens.", "Answer"])
    elif acak == 3:
        Question = (["The graph below shows how the number of bulbs in a circuit affects the brightness of a bulb, when the number of batteries used in the circuit remains the same.", "Based on the graph above, answer the following questions.\nHow did the number of bulbs connected in the circuit affect the brightness of the bulbs?", "Answer..."])
    try:
        tab = driver.find_element_by_xpath('//li[@role="presentation"]')
        if Type == "MCQs":
            tab.find_element_by_xpath('//a[@href="#multipleChoice"]').click()
        elif Type == "OEQs":
            tab.find_element_by_xpath('//a[@href="#openEndedQuestion"]').click()
        time.sleep(2)
        try:
            pick = driver.find_element_by_xpath('//form[@name="question_oeq" and @id="submit-question-oeq"]')
            count = 0
            for template in pick.find_elements_by_xpath('//div[@class="block-grid pointer template-selected"]'):
                if template.is_displayed():
                    count = count + 1
            if TempTipe <= count:
                try:
                    pick = driver.find_element_by_xpath('//form[@name="question_oeq" and @id="submit-question-oeq"]')
                    no = 1
                    for template in pick.find_elements_by_xpath('//div[@class="block-grid pointer template-selected"]'):
                        if template.is_displayed():
                            if no == TempTipe:
                                template.click()
                                time.sleep(1)
                                break
                            no = no + 1
                    selectric = pick.find_elements_by_xpath('//div[@class="selectric"]')
                    selectric[2].click()
                    time.sleep(2)
                    logging = len(pick.find_elements_by_xpath('//div[@class="selectric-scroll"]'))
                    scroll = pick.find_elements_by_xpath('//div[@class="selectric-scroll"]')[2]
                    scroll.find_element_by_xpath('//*[@id="submit-question-oeq"]/div[3]/div[2]/div[1]/div/div/div[3]/div/ul/li[contains(text(), "'+School+'")]').click()
                    for level_ in pick.find_elements_by_xpath('//div[@class="radio radio-danger radio-inline"]'):
                        if level_.is_displayed():
                            if level_.text == Level:
                                level_.click()
                                break
                    for exam_ in pick.find_elements_by_xpath('//div[@class="radio radio-danger radio-inline"]'):
                        if exam_.is_displayed():
                            if exam_.text == Examination:
                                exam_.click()
                                break
                    if "NA" in Level or "NA" in Examination:
                        pass
                    else:
                        pick.find_element_by_xpath('//input[@id="question_oeq_number" and @name="question_oeq[number]"]').send_keys(Number)
                    edit = 0
                    pick.find_element_by_xpath('//a[@class="btn btn--dark" and @id="add-sub-quetion-button"]').click()
                    time.sleep(2)
                    for editor in driver.find_elements_by_xpath('//div[@class="fr-element fr-view" and @contenteditable="true"]'):
                        if editor.is_displayed():
                            hover = ActionChains(driver).move_to_element(editor)
                            hover.perform()
                            editor.click()
                            editor.send_keys(Question[edit])
                            edit = edit + 1
                    for score in driver.find_elements_by_xpath('//input[@class="form-control score-data" and @name="score[]"]'):
                        if score.is_displayed():
                            score.send_keys(1)
                    for button in driver.find_elements_by_xpath('//button[@class="btn btn--blue button-submit-oeq" and contains(text(), "SAVE")]'):
                        if button.is_displayed():
                            button.click()
                            time.sleep(5)
                        try:
                            alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                            logging = alert.text
                            if "Successfully" in logging:
                                status = "PASS"
                            else:
                                status = "FAIL"
                        except Exception as e:
                            raise
                except Exception as e:
                    raise
            else:
                logging = "tidak ada template "+str(TempTipe)
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status
