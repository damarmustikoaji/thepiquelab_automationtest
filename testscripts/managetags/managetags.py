import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

logging = None
status = None

def NewTags(driver, TAGS, ChangeTags):
    driver = driver
    time.sleep(5)
    status = "FAIL"
    try:
        manage = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'cardtags')))
        tag = manage.find_elements_by_xpath('//div[@class="tags__block"]')
        if TAGS in manage.text or ChangeTags in manage.text:
            logging = "Data sudah ada"
        else:
            field = driver.find_element_by_id('tag_name')
            field.send_keys(TAGS)
            driver.find_element_by_xpath('//button[@id="btn-add" and contains(text(), "SAVE")]').click()
            time.sleep(3)
            try:
                manage = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'cardtags')))
                tag = manage.find_elements_by_xpath('//div[@class="tags__block"]')
                for data in tag:
                    if TAGS in data.text:
                        logging = "Success Added tag"
                        status = "PASS"
            except Exception as e:
                raise
    except Exception as e:
        logging = "Data not found"
    return driver, logging, status

def EditTags(driver, TAGS, ChangeTags):
    driver = driver
    time.sleep(5)
    status = "FAIL"
    try:
        manage = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'cardtags')))
        if ChangeTags in manage.text:
            logging = "Data sudah ada"
        else:
            if TAGS in manage.text:
                logging = "Siap edit"
                tag = manage.find_elements_by_xpath('//div[@class="tags__block"]')
                #no = 0
                for value in tag:
                    if value.text == TAGS:
                        hover = ActionChains(driver).move_to_element(value)
                        hover.perform()
                        time.sleep(2)
                        try:
                            aksi = value.find_element_by_xpath('//div[@class="block__action"]')
                            for title in value.find_elements_by_xpath('//span'):
                                if title.text == "edit":
                                    logging = title.text
                                    title.click()
                                    try:
                                        field = driver.find_element_by_id('tag_name')
                                        field.send_keys("Edited")
                                        driver.find_element_by_xpath('//button[@id="btn-update" and contains(text(), "UPDATE")]').click()
                                        status = "PASS"
                                    except Exception as e:
                                        raise
                                    time.sleep(3)
                        except Exception as e:
                            raise
                        break
            else:
                logging = "Belum ada data yang bisa di edit"
    except Exception as e:
        logging = "Data not found"
    return driver, logging, status

def DeleteTags(driver, ChangeTags):
    driver = driver
    time.sleep(5)
    status = "FAIL"
    try:
        manage = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, 'cardtags')))
        tag = manage.find_elements_by_xpath('//div[@class="tags__block"]')
        if ChangeTags in manage.text:
            logging = "Siap delete"
            tag = manage.find_elements_by_xpath('//div[@class="tags__block"]')
            for value in tag:
                if value.text == ChangeTags:
                    hover = ActionChains(driver).move_to_element(value)
                    hover.perform()
                    time.sleep(2)
                    try:
                        aksi = value.find_element_by_xpath('//div[@class="block__action"]')
                        for title in value.find_elements_by_xpath('//span'):
                            if title.text == "trash":
                                logging = title.text
                                title.click()
                                status = "PASS"
                                time.sleep(3)
                    except Exception as e:
                        raise
                    break
        else:
            logging = "Tidak ada data "+ChangeTags
    except Exception as e:
        logging = "Data not found"
    return driver, logging, status
