import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import WebDriverException

def FindQuestion(driver, Level, School, Year, Examination, QuestionType):
    log = None
    status = None
    driver = driver
    wait = WebDriverWait(driver, 10)
    try:
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Level+'")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+QuestionType+'")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "'+Examination+'")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Used")]').click()
        driver.find_element_by_xpath('//div[@class="checkbox checkbox-primary checkbox-inline"]/label[contains(text(), "Unused")]').click()
        driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue" and contains(text(), "GENERATE QUESTIONS")]').click()
        time.sleep(2)
        try:
            modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.CSS_SELECTOR, 'div.swal2-container.swal2-center.swal2-fade.swal2-shown')))
            log = modal.text
            status = "FAIL"
        except Exception as e:
            try:
                log = driver.find_element_by_xpath('//div[@class="col-md-3 pad-right-0"]').text
                status = "PASS"
            except Exception as e:
                raise
    except Exception as e:
        raise
    return driver, log, status
