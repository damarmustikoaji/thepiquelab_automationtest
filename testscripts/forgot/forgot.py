import time
from selenium import webdriver

logging = None
status = None

def ForgotPassword(driver, EMAIL):
    driver = driver
    try:
        email = driver.find_element_by_id("forgot_email")
        email.send_keys(EMAIL)
        #driver.save_screenshot('capture/login_pique.png')
        if email.get_attribute('value') == EMAIL:
            logging = email.get_attribute('value')
            driver.find_element_by_xpath('//button[@type="submit" and @class="btn btn-default btn-block btn--pique"]').click()
            time.sleep(2)
            try:
                alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                logging = alert.text
                if "Please Check your email!" in logging:
                    status = "PASS"
                else:
                    status = "FAIL"
            except Exception as e:
                alert = driver.find_element_by_xpath('//div[@class="alert alert-danger"]')
                logging = alert.text
                status= "FAIL"
        else:
            logging = email.get_attribute('value')
            status = "FAIL"
        #time.sleep(5)
    except Exception as e:
        raise
    return driver, logging, status
