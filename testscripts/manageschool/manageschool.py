import time
from selenium import webdriver
from selenium.common.exceptions import NoAlertPresentException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

logging = None
status = None

def CreateSchool(driver, NameSchool, CodeSchool):
    driver = driver
    try:
        driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue-outline"]').click()
        try:
            namaschool = driver.find_element_by_id("school_name")
            namaschool.send_keys(NameSchool)
            kodeschool = driver.find_element_by_id("school_code")
            kodeschool.send_keys(CodeSchool)
            #alamatschool = driver.find_element_by_id("school_address")
            #alamatschool.send_keys(AddressSchool)
            if namaschool.get_attribute('value') == NameSchool and kodeschool.get_attribute('value') == CodeSchool:
                driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue"]').click()
                try:
                    alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                    logging = alert.text
                    if "succeeded" in logging:
                        status = "PASS"
                    else:
                        status = "FAIL"
                except Exception as e:
                    raise
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def DeleteSchool(driver, NameSchool):
    driver = driver
    try:
        baris = driver.find_elements_by_tag_name('tr')
        for sekolah in baris:
            temp = sekolah
            if NameSchool in temp.text:
                try:
                    logging = temp.text
                    temp.find_element_by_link_text("Delete").click()
                    try:
                        modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
                        logging = modal.text
                        modal.find_element_by_xpath('//button[@type="button" and @class="swal2-confirm swal2-styled"]').click()
                        time.sleep(3)
                        status = "PASS"
                    except Exception as e:
                        raise
                    break
                except Exception as e:
                    raise
            else:
                logging = "Not found: "+NameSchool
                status = "FAIL"
    except Exception as e:
        raise
    return driver, logging, status

def EditSchool(driver, NameSchool, CodeSchool):
    driver = driver
    try:
        baris = driver.find_elements_by_tag_name('tr')
        for sekolah in baris:
            temp = sekolah
            if NameSchool in temp.text:
                try:
                    logging = temp.text
                    temp.find_element_by_link_text("Edit").click()
                    time.sleep(2)
                    try:
                        #NameSchool = NameSchool+"Edited"
                        #CodeSchool = CodeSchool+"Edited"
                        #AddressSchool = AddressSchool+"Edited"
                        namaschool = driver.find_element_by_id("school_name")
                        #namaschool.clear()
                        namaschool.send_keys("Edited")
                        namaschool.click()
                        time.sleep(2)
                        #kodeschool = driver.find_element_by_id("school_code")
                        #kodeschool.clear()
                        #kodeschool.send_keys("Edited")
                        #kodeschool.click()
                        #time.sleep(2)
                        #alamatschool = driver.find_element_by_id("school_address")
                        #alamatschool.clear()
                        #alamatschool.send_keys("Edited")
                        #alamatschool.click()
                        #time.sleep(2)
                        #if namaschool.get_attribute('value') == NameSchool and kodeschool.get_attribute('value') == CodeSchool and alamatschool.get_attribute('value') == AddressSchool:
                        button = driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue" and contains(text(), "SAVE")]')#.click()
                        ActionChains(driver).move_to_element(button).perform()
                        time.sleep(2)
                        button.click()
                        try:
                            alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                            logging = alert.text
                            if "succeeded" in logging:
                                status = "PASS"
                            else:
                                status = "FAIL"
                        except Exception as e:
                            raise
                    except Exception as e:
                        raise
                    break
                except Exception as e:
                    raise
            else:
                logging = "Not found: "+NameSchool
                status = "FAIL"
    except Exception as e:
        raise
    return driver, logging, status
