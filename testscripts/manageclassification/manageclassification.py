import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

logging = None
status = None

def NewClassification(driver, Type, ClassificationName, Description, Level, Parent):
    driver = driver
    try:
        tab = driver.find_element_by_xpath('//li[@role="presentation"]')
        if Type == "MCQs":
            tab.find_element_by_xpath('//a[@href="#multipleChoice"]').click()
            time.sleep(2)
            Name_Classification = driver.find_element_by_id("category_mcq_name")
            Description_Classification = driver.find_element_by_id("category_mcq_description")
            button = driver.find_element_by_id('btn-submit-mcq')
        elif Type == "OEQs":
            tab.find_element_by_xpath('//a[@href="#openEndedQuestion"]').click()
            time.sleep(2)
            Name_Classification = driver.find_element_by_id("category_oeq_name")
            Description_Classification = driver.find_element_by_id("category_oeq_description")
            button = driver.find_element_by_id('btn-submit-oeq')
        Name_Classification.send_keys(ClassificationName)
        Description_Classification.send_keys(Description)
        for level_ in driver.find_elements_by_xpath('//div[@class="radio radio-danger radio-inline"]'):
            if level_.is_displayed():
                if level_.text == Level:
                    level_.click()
                    break
        if Name_Classification.get_attribute('value') == ClassificationName and Description_Classification.get_attribute('value') == Description:
            button.click()
            try:
                alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                logging = alert.text
                if "succeeded" in logging:
                    status = "PASS"
                else:
                    status = "FAIL"
            except Exception as e:
                raise
    except Exception as e:
        raise
    return driver, logging, status

def DeleteClassification(driver, Type, ClassificationName):
    driver = driver
    try:
        tab = driver.find_element_by_xpath('//ul[@class="nav nav-tabs nav-tabs--sortQuestion" and @id="Tabs"]')
        konten = driver.find_element_by_xpath('//div[@class="tab-content"]')
        if Type == "MCQs":
            time.sleep(2)
            tab.find_element_by_xpath('//a[@href="#multipleChoice"]').click()
            #time.sleep(2)
            tab_ = driver.find_element_by_xpath('//div[@id="multipleChoice"]')
        elif Type == "OEQs":
            time.sleep(2)
            tab.find_element_by_xpath('//a[@href="#openEndedQuestion"]').click()
            time.sleep(2)
            tab_ = konten.find_element_by_xpath('//div[@class="tab-pane fade active in" and @id="openEndedQuestion"]')
        try:
            data = tab_.find_elements_by_xpath('//tr')
            for kategori in data:
                if ClassificationName in kategori.text:
                    hover = ActionChains(driver).move_to_element(kategori)
                    hover.perform()
                    time.sleep(2)
                    tmp = kategori.find_elements_by_tag_name('td')[4]
                    tmp.find_elements_by_tag_name('a')[1].click()
                    try:
                        modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
                        logging = modal.text
                        modal.find_element_by_xpath('//button[@class="swal2-confirm swal2-styled"]').click()
                        time.sleep(2)
                        try:
                            logging = driver.find_element_by_xpath('//div[@class="alert alert-success"]').text
                            if "succeeded" in logging:
                                status = "PASS"
                            else:
                                status = "FAIL"
                        except Exception as e:
                            raise
                        break
                    except Exception as e:
                        raise
                else:
                    logging = "Not found: "+ClassificationName
                    status = "FAIL"
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def EditClassification(driver, Type, ClassificationName):
    driver = driver
    try:
        tab = driver.find_element_by_xpath('//ul[@class="nav nav-tabs nav-tabs--sortQuestion" and @id="Tabs"]')
        konten = driver.find_element_by_xpath('//div[@class="tab-content"]')
        if Type == "MCQs":
            time.sleep(2)
            tab.find_element_by_xpath('//a[@href="#multipleChoice"]').click()
            #time.sleep(2)
            tab_ = driver.find_element_by_xpath('//div[@id="multipleChoice"]')
        elif Type == "OEQs":
            time.sleep(2)
            tab.find_element_by_xpath('//a[@href="#openEndedQuestion"]').click()
            time.sleep(2)
            tab_ = konten.find_element_by_xpath('//div[@class="tab-pane fade active in" and @id="openEndedQuestion"]')
        try:
            time.sleep(1)
            data = tab_.find_elements_by_xpath('//tr')
            for kategori in data:
                if ClassificationName in kategori.text:
                    hover = ActionChains(driver).move_to_element(kategori)
                    hover.perform()
                    time.sleep(2)
                    tmp = kategori.find_elements_by_tag_name('td')[4]
                    tmp.find_elements_by_tag_name('a')[0].click()
                    try:
                        time.sleep(2)
                        driver.find_element_by_xpath('//input[@id="category_mcq_name" and @name="category_mcq[name]"]').send_keys("Edited")
                        driver.find_element_by_xpath('//textarea[@id="category_mcq_description" and @name="category_mcq[description]"]').send_keys("Edited")
                        driver.find_element_by_xpath('//button[@class="btn btn--blue margin-top-3" and @id="btn-update-mcq"]').click()
                        time.sleep(2)
                        try:
                            alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
                            logging = alert.text
                            if "succeeded" in logging:
                                status = "PASS"
                            else:
                                status = "FAIL"
                        except Exception as e:
                            raise
                    except Exception as e:
                        raise
                    break
                else:
                    logging = "Not found: "+ClassificationName
                    status = "FAIL"
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status
