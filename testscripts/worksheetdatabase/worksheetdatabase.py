import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.action_chains import ActionChains

logging = None
status = None

def CreateFolder(driver, Name):
    driver = driver
    no = 0
    try:
        driver.find_element_by_xpath('//input[@name="name" and @class="form-control form-control__borders"]').send_keys(Name)
        driver.find_element_by_xpath('//button[@class="btn btn--blue" and contains(text(), "CREATE FOLDER")]').click()
        try:
            alert = driver.find_element_by_xpath('//div[@class="alert alert-success"]')
            logging = alert.text
            if "Successfully" in logging:
                status = "PASS"
            else:
                status = "FAIL"
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def EditFolder(driver, Name):
    driver = driver
    no = 0
    try:
        table = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH, '//table[@class="treetable"]')))
        folder = table.find_element_by_xpath('//div[contains(text(), "'+Name+'")]')
        hover = ActionChains(driver).move_to_element(folder)
        hover.perform()
        logging = folder.text
        aksi = folder.find_element_by_xpath('//div[@class="pull-right"]')
        for title in aksi.find_elements_by_xpath('//span'):
            if title.text == "pen":
                title.click()
                try:
                    modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal-wide swal2-show"]')))
                    logging = modal.text
                    modal.find_element_by_xpath('//input[@class="swal2-input"]').send_keys("Edited")
                    modal.find_element_by_xpath('//button[@class="swal2-confirm swal2-styled"]').click()
                    time.sleep(5)
                    status = "PASS"
                except Exception as e:
                    raise
                break
    except Exception as e:
        raise
    return driver, logging, status

def DeleteFolder(driver, Name):
    driver = driver
    no = 0
    try:
        table = WebDriverWait(driver, 60).until(EC.presence_of_element_located((By.XPATH, '//table[@class="treetable"]')))
        folder = table.find_element_by_xpath('//div[contains(text(), "'+Name+'")]')
        hover = ActionChains(driver).move_to_element(folder)
        hover.perform()
        logging = folder.text
        aksi = folder.find_element_by_xpath('//div[@class="pull-right"]')
        for title in aksi.find_elements_by_xpath('//span'):
            if title.text == "trash":
                title.click()
                try:
                    modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
                    logging = modal.text
                    modal.find_element_by_xpath('//button[@class="swal2-confirm swal2-styled"]').click()
                    time.sleep(5)
                    status = "PASS"
                except Exception as e:
                    raise
                break
    except Exception as e:
        raise
    return driver, logging, status
