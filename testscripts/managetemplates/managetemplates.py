import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

logging = None
status = None

def CreateTemplate(driver, Template, Tipe):
    driver = driver
    no = 0
    try:
        driver.find_element_by_xpath('//button[@class="btn btn-default btn--blue-outline"]').click()
        time.sleep(2)
        try:
            tab = driver.find_element_by_xpath('//li[@role="presentation"]')
            if Template == "MCQs":
                tab.find_element_by_xpath('//a[@href="#multipleChoice"]').click()
            elif Template == "OEQs":
                tab.find_element_by_xpath('//a[@href="#openEndedQuestion"]').click()
            time.sleep(5)
            try:
                insergrid = driver.find_element_by_xpath('//div[@class="choose-grid"]')
                for grid in insergrid.find_elements_by_xpath('//div[@class="grid__box"]'):
                    if no == Tipe:
                        grid.click()
                        time.sleep(2)
                        break
                    no = no + 1
                driver.find_element_by_xpath('//button[@type="submit" and @class="btn btn-default btn--blue"]').click()
                status = "PASS"
            except Exception as e:
                raise
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def EditTemplate(driver, Template, Tipe):
    driver = driver
    logging = None
    status = None
    no = 1
    try:
        tab = driver.find_element_by_xpath('//li[@role="presentation"]')
        if Template == "MCQs":
            tab.find_element_by_xpath('//a[@href="#multipleChoice"]').click()
        elif Template == "OEQs":
            tab.find_element_by_xpath('//a[@href="#openEndedQuestion"]').click()
        time.sleep(2)
        try:
            tab = driver.find_element_by_xpath('//div[@class="tab-pane fade in active" and @id="multipleChoice"]')
            picka = tab.find_element_by_xpath('//div[@class="flex-container wrap"]')
            count = 0
            for template in picka.find_elements_by_xpath('//div[@class="block-grid"]'):
                if template.is_displayed():
                    count = count + 1
            count = count - 1
            if count > 0:
                try:
                    no = 0
                    for template in picka.find_elements_by_xpath('//div[@class="block-grid"]'):
                        if template.is_displayed():
                            if no == count:
                                template.find_elements_by_xpath('//a[@class="btn btn-link btn-block" and contains(text(), "Edit")]')[no].click()
                                time.sleep(5)
                                try:
                                    templ = 0
                                    insergrid = driver.find_element_by_xpath('//div[@class="choose-grid"]')
                                    for grid in insergrid.find_elements_by_xpath('//div[@class="grid__box"]'):
                                        if templ == Tipe:
                                            grid.click()
                                            time.sleep(2)
                                            break
                                        templ = templ + 1
                                    driver.find_element_by_xpath('//button[@type="submit" and @class="btn btn-default btn--blue"]').click()
                                    status = "PASS"
                                except Exception as e:
                                    raise
                                break
                            no = no + 1
                except Exception as e:
                    raise
            else:
                logging = "Template not found"
                status = "FAIL"
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status

def DeleteTemplate(driver, Template):
    driver = driver
    logging = None
    status = None
    no = 1
    try:
        tab = driver.find_element_by_xpath('//li[@role="presentation"]')
        if Template == "MCQs":
            tab.find_element_by_xpath('//a[@href="#multipleChoice"]').click()
        elif Template == "OEQs":
            tab.find_element_by_xpath('//a[@href="#openEndedQuestion"]').click()
        time.sleep(2)
        try:
            tab = driver.find_element_by_xpath('//div[@class="tab-pane fade in active" and @id="multipleChoice"]')
            picka = tab.find_element_by_xpath('//div[@class="flex-container wrap"]')
            count = 0
            for template in picka.find_elements_by_xpath('//div[@class="block-grid"]'):
                if template.is_displayed():
                    count = count + 1
            count = count - 1
            if count > 0:
                try:
                    no = 0
                    for template in picka.find_elements_by_xpath('//div[@class="block-grid"]'):
                        if template.is_displayed():
                            if no == count:
                                template.find_elements_by_xpath('//a[@href="#" and @onclick="return confirmDelete(this);"]')[no].click()
                                try:
                                    modal = WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.XPATH, '//div[@class="swal2-popup swal2-modal swal2-show"]')))
                                    logging = modal.text
                                    modal.find_element_by_xpath('//button[@type="button" and @class="swal2-confirm swal2-styled"]').click()
                                    time.sleep(2)
                                    status = "PASS"
                                except Exception as e:
                                    raise
                                break
                            no = no + 1
                except Exception as e:
                    raise
            else:
                logging = "Template not found"
                status = "FAIL"
        except Exception as e:
            raise
    except Exception as e:
        raise
    return driver, logging, status
