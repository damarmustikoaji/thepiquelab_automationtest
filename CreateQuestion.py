import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.createquestion.createquestion import CreateQuestionMCQ
from testscripts.createquestion.createquestion import CreateQuestionOEQ
from testscripts.home.reviewquestion import ReviewQuestion

import random
from random import randint

LOGIN = None
Type1 = "MCQs"
TempTipe1 = 1
School1 = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year1 = random.choice([2015, 2016, 2017])
Level1 = random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination1 = random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])
Number1 = randint(0, 35)

Type2 = "OEQs"
TempTipe2 = 1
School2 = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year2 = random.choice([2015, 2016, 2017])
Level2 = random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination2 = random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])
Number2 = randint(0, 35)

class CreateQuestiontest(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        #assert "Login - Piquelabs" in self.driver.title
        self.assertEqual(self.driver.title,"Login - Tutoract")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_f_create_question_mcqs(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/questions/")
            driver, logging, status = CreateQuestionMCQ(self.driver, Type1, TempTipe1, School1, Year1, Level1, Examination1, Number1)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_g_review_question(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/dashboard")
            driver, logging, status = ReviewQuestion(self.driver, self.EMAIL)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_h_create_question_oeqs(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/questions/")
            driver, logging, status = CreateQuestionOEQ(self.driver, Type2, TempTipe2, School2, Year2, Level2, Examination2, Number2)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_i_review_question(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/dashboard")
            driver, logging, status = ReviewQuestion(self.driver, self.EMAIL)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_x_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        CreateQuestiontest.BROWSER = sys.argv.pop()
        CreateQuestiontest.SERVER = sys.argv.pop()
        if "http" not in CreateQuestiontest.SERVER:
            CreateQuestiontest.SERVER = "http://"+CreateQuestiontest.SERVER
        CreateQuestiontest.PASSWORD = sys.argv.pop()
        CreateQuestiontest.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(CreateQuestiontest)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
