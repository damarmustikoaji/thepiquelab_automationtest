import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.randomstring.randomstring import randomword
from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.manageschool.manageschool import CreateSchool
from testscripts.manageschool.manageschool import DeleteSchool
from testscripts.manageschool.manageschool import EditSchool

import random
from random import randint

LOGIN = None
CreateSchool_ = None
school = randomword(4)
NameSchool = "SMA Negeri "+school.upper()+" "+str(randint(1, 100))
CodeSchool = school.upper()
#AddrSchool = "Jalan "+school+" No. "+str(randint(1, 100))+" Yogyakarta, Indonesia"

class ManageSchool(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        #assert "Login - Piquelabs" in self.driver.title
        self.assertEqual(self.driver.title,"Login - Tutoract")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_b_manage_school_create(self):
        global LOGIN, CreateSchool_
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/schools/")
            driver, logging, status = CreateSchool(self.driver, NameSchool, CodeSchool)
            print logging
            print status
            CreateSchool_ = status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_c_manage_school_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/schools/")
            driver, logging, status = EditSchool(self.driver, NameSchool, CodeSchool)
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_d_manage_school_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/schools/")
            driver, logging, status = DeleteSchool(self.driver, NameSchool+"Edited")
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_x_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        ManageSchool.BROWSER = sys.argv.pop()
        ManageSchool.SERVER = sys.argv.pop()
        if "http" not in ManageSchool.SERVER:
            ManageSchool.SERVER = "http://"+ManageSchool.SERVER
        ManageSchool.PASSWORD = sys.argv.pop()
        ManageSchool.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(ManageSchool)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
