### Automation Testing The Pique Lab apps

### Requrements
- Python => 2.7.10
- Selenium webdriver == 3.6.0

### Arguments
- $: python Test.py <email> <password> <url> <browser>
- EX$: python Test.py admin@piquelab.sg admin http://piquelab.wonderlabs.io PhantomJS

#### Note :
- url = http://piquelab.wonderlabs.io
- browser = PhantomJS / Chrome / Firefox / Default: Chrome

### Install PhantomJS
- Install Required Packages
```
$ sudo apt-get update
$ sudo apt-get install build-essential chrpath libssl-dev libxft-dev
$ sudo apt-get install libfreetype6 libfreetype6-dev libfontconfig1 libfontconfig1-dev
```
- Install PhantomJS
```
$ wget https://bitbucket.org/ariya/phantomjs/downloads/phantomjs-2.1.1-linux-x86_64.tar.bz2
$ tar xvjf phantomjs-2.1.1-linux-x86_64.tar.bz2 -C /usr/local/share/
```

```
sudo ln -s /usr/local/share/phantomjs-x.y.z-linux-x86_64/ /usr/local/share/phantomjs
sudo ln -s /usr/local/share/phantomjs/bin/phantomjs /usr/local/bin/phantomjs
```

- Verify
```
$ phantomjs --version
```
- Reference
- https://tecadmin.net/install-phantomjs-on-ubuntu/
- http://attester.ariatemplates.com/usage/phantom.html
