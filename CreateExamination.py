import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.createexamination.createexam import CreateExam

import random
from random import randint

LOGIN = None
School = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year = random.choice([2015, 2016, 2017])
Level = random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination = random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])
if Level == Examination:
    Examination = random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims"])
MCQs = 1#randint(1, 3)
OEQs = 1#randint(1, 3)

class CreateQuestiontest(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        #assert "Login - Piquelabs" in self.driver.title
        #self.assertEqual(self.driver.title,"Login - Piquelabs")
        #self.driver.save_screenshot("image.jpg")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_b_create_exam(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/examinations/")
            driver, logging, status = CreateExam(self.driver, School, Year, Level, Examination, MCQs, OEQs)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_x_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        CreateQuestiontest.BROWSER = sys.argv.pop()
        CreateQuestiontest.SERVER = sys.argv.pop()
        if "http" not in CreateQuestiontest.SERVER:
            CreateQuestiontest.SERVER = "http://"+CreateQuestiontest.SERVER
        CreateQuestiontest.PASSWORD = sys.argv.pop()
        CreateQuestiontest.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(CreateQuestiontest)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
