import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.browser.browser import DriverBrowser
from testscripts.forgot.forgot import ForgotPassword

class ForgotPasswordtest(unittest.TestCase):

    SERVER = None
    EMAIL = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_y_Forgot_Password(self):
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/forgot")
        driver, logging, status = ForgotPassword(self.driver, self.EMAIL)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 4:
        ForgotPasswordtest.BROWSER = sys.argv.pop()
        ForgotPasswordtest.SERVER = sys.argv.pop()
        if "http" not in ForgotPasswordtest.SERVER:
            ForgotPasswordtest.SERVER = "http://"+ForgotPasswordtest.SERVER
        ForgotPasswordtest.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(ForgotPasswordtest)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
