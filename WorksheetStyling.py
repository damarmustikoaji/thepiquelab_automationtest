import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.styling.worksheetstyling import NewCover
from testscripts.styling.worksheetstyling import DeleteCover
from testscripts.styling.worksheetstyling import EditCover
from testscripts.styling.worksheetstyling import NewFooter
from testscripts.styling.worksheetstyling import DeleteFooter

import random
from random import randint
from testscripts.randomstring.randomstring import randomword

LOGIN = None
Name = "Cover"
Styling = "Cover"
Text_Footer = "Copyright 2018 - 2020: The "+randomword(4).upper()+". All rights reserved."
Img_Footer = "/Users/wonderlabs/damar/Belajar/thepiquelab_automationtest/file/Footer.png"

class WorksheetStyling(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        #assert "Login - Piquelabs" in self.driver.title
        self.assertEqual(self.driver.title,"Login - Tutoract")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_b_worksheet_styling_new(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = NewCover(self.driver, Name)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_c_worksheet_styling_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = EditCover(self.driver)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def _d_worksheet_styling_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = DeleteCover(self.driver)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_e_worksheet_footer_new(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = NewFooter(self.driver, Text_Footer, Img_Footer)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_f_worksheet_footer_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = DeleteFooter(self.driver, Text_Footer)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_x_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        WorksheetStyling.BROWSER = sys.argv.pop()
        WorksheetStyling.SERVER = sys.argv.pop()
        if "http" not in WorksheetStyling.SERVER:
            WorksheetStyling.SERVER = "http://"+WorksheetStyling.SERVER
        WorksheetStyling.PASSWORD = sys.argv.pop()
        WorksheetStyling.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(WorksheetStyling)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
