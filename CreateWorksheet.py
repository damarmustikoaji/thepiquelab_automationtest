import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.createworksheet.createworksheet import CreateWorksheet

import random
from random import randint

LOGIN = None
WorksheetName = "Test Worksheet "+str(randint(1, 20))
School = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year = random.choice([2015, 2016, 2017])
Level = "Primary 3"#random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination = "CA1"#random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])

class CreateWorksheettest(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        #assert "Login - Piquelabs" in self.driver.title
        self.assertEqual(self.driver.title,"Login - Tutoract")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_b_create_worksheet(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/worksheets/new")
            driver, logging, status = CreateWorksheet(self.driver, WorksheetName, School, Year, Examination, Level)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_x_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        CreateWorksheettest.BROWSER = sys.argv.pop()
        CreateWorksheettest.SERVER = sys.argv.pop()
        if "http" not in CreateWorksheettest.SERVER:
            CreateWorksheettest.SERVER = "http://"+CreateWorksheettest.SERVER
        CreateWorksheettest.PASSWORD = sys.argv.pop()
        CreateWorksheettest.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(CreateWorksheettest)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
