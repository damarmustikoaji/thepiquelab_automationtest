import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner

from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.managetemplates.managetemplates import CreateTemplate
from testscripts.managetemplates.managetemplates import EditTemplate
from testscripts.managetemplates.managetemplates import DeleteTemplate

LOGIN = None
Template = "MCQs"
Tipe = 0
Tipe2 = 2

class ManageTemplates(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        #assert "Login - Piquelabs" in self.driver.title
        self.assertEqual(self.driver.title,"Login - Tutoract")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_b_manage_templates_create(self):
        global LOGIN, Fullname_
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/templates")
            driver, logging, status = CreateTemplate(self.driver, Template, Tipe)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_c_manage_templates_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/templates")
            driver, logging, status = EditTemplate(self.driver, Template, Tipe2)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_d_manage_templates_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/templates")
            driver, logging, status = DeleteTemplate(self.driver, Template)
            print logging, status
            self.driver.get(self.SERVER+"/templates")
            driver, logging, status = DeleteTemplate(self.driver, Template)
            print logging, status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_x_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        ManageTemplates.BROWSER = sys.argv.pop()
        ManageTemplates.SERVER = sys.argv.pop()
        if "http" not in ManageTemplates.SERVER:
            ManageTemplates.SERVER = "http://"+ManageTemplates.SERVER
        ManageTemplates.PASSWORD = sys.argv.pop()
        ManageTemplates.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    #suite = unittest.TestLoader().loadTestsFromTestCase(ManageTemplates)
    #unittest.TextTestRunner(verbosity=0).run(suite)
    HTMLTestRunner.main()
