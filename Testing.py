import sys
import unittest
import time
from selenium import webdriver
from HTMLTestRunner_report import HTMLTestRunner
import random
from random import randint

from testscripts.randomstring.randomstring import randomword
from testscripts.browser.browser import DriverBrowser
from testscripts.login.login import Login
from testscripts.logout.logout import Logout
from testscripts.createquestion.createquestion import CreateQuestionMCQ
from testscripts.createquestion.createquestion import CreateQuestionOEQ
from testscripts.home.reviewquestion import ReviewQuestion
from testscripts.createexamination.createexam import CreateExam
from testscripts.questiondatabase.questiondb import FindQuestionDB
from testscripts.sortquestion.sortquestion import FindQuestion
from testscripts.managetemplates.managetemplates import CreateTemplate
from testscripts.managetemplates.managetemplates import EditTemplate
from testscripts.managetemplates.managetemplates import DeleteTemplate
from testscripts.manageclassification.manageclassification import NewClassification
from testscripts.manageclassification.manageclassification import DeleteClassification
from testscripts.manageclassification.manageclassification import EditClassification
from testscripts.managetags.managetags import NewTags
from testscripts.managetags.managetags import DeleteTags
from testscripts.managetags.managetags import EditTags
from testscripts.createworksheet.createworksheet import CreateWorksheet
from testscripts.worksheetdatabase.worksheetdatabase import CreateFolder
from testscripts.worksheetdatabase.worksheetdatabase import DeleteFolder
from testscripts.worksheetdatabase.worksheetdatabase import EditFolder
from testscripts.styling.worksheetstyling import NewCover
from testscripts.styling.worksheetstyling import DeleteCover
from testscripts.styling.worksheetstyling import EditCover
from testscripts.styling.worksheetstyling import NewFooter
from testscripts.styling.worksheetstyling import DeleteFooter
from testscripts.manageschool.manageschool import CreateSchool
from testscripts.manageschool.manageschool import DeleteSchool
from testscripts.manageschool.manageschool import EditSchool
from testscripts.manageusers.manageusers import InviteUser
from testscripts.manageusers.manageusers import DeleteUser
from testscripts.manageusers.manageusers import EditUser
from testscripts.managecentres.managecentres import CreateCentre
#from testscripts.managecentres.managecentres import DeleteCentre
from testscripts.managecentres.managecentres import EditCentre
from testscripts.forgot.forgot import ForgotPassword

LOGIN = None

Type1 = "MCQs"
TempTipe1 = 1
School1 = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year1 = random.choice([2015, 2016, 2017])
Level1 = "Primary 3"#random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination1 = "CA1"#random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])
Number1 = randint(0, 35)

Type2 = "OEQs"
TempTipe2 = 1
School2 = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year2 = random.choice([2015, 2016, 2017])
Level2 = "Primary 3"#random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination2 = "CA1"#random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])
Number2 = randint(0, 35)

School_exam = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year_exam = random.choice([2015, 2016, 2017])
Level_exam = "Primary 3"#random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination_exam = "CA1"#random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])
if Level_exam == Examination_exam:
    Examination_exam = random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims"])
MCQs_exam = 1#randint(1, 3)
OEQs_exam = 1#randint(1, 3)

School_qb = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year_qb = random.choice([2015, 2016, 2017])
Level_qb = "Primary 3"#random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination_qb = "CA1"#random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])
Number_qb = randint(0, 100)
QuestionType_qb = random.choice(["Multiple Choice Question", "Open-Ended Question"])

School_sq = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year_sq = random.choice([2015, 2016, 2017])
Level_sq = "Primary 3"#random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination_sq = "CA1"#random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])
QuestionType_sq = random.choice(["Multiple Choice Question", "Open-Ended Question"])

Template_mtmp = "MCQs"
Tipe_mtmp = 0
Tipe2_mtmp = 2

NameClassification__mclf = None
TypeClass_mclf = "MCQs"#random.choice(["MCQs", "OEQs"])
NameClassification_mclf = random.choice(["Physics", "Biology", "Bahasa", "Maths", "Penjaskes"])
DescritionClassification_mclf = random.choice(["Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium.", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labor.", "At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum."])
LevelClassification_mclf = random.choice(["Primary "+str(randint(3, 6)), "NA"])
ParentClassification_mclf = ""

Tags_mtg = "Stella"
ChangeTags_mtg = Tags_mtg+"Edited"

WorksheetName_cwork = "Test Worksheet "+str(randint(1, 20))
School_cwork = random.choice(["Wonderlabs (WL)", "Padmanaba (PMA)", "Delayota (DLA)"])
Year_cwork = random.choice([2015, 2016, 2017])
Level_cwork = "Primary 3"#random.choice(["Primary "+str(randint(3, 6)), "NA"])
Examination_cwork = "CA1"#random.choice(["CA1", "SA1", "CA2", "SA2", "Prelims", "NA"])

Folder = "Test_Folder"

Name_wstly = "Cover"
Styling_wstly = "Cover"
Text_Footer_wstly = "Copyright 2018 - 2020: The "+randomword(4).upper()+". All rights reserved."
Img_Footer_wstly = "/Users/wonderlabs/damar/Belajar/thepiquelab_automationtest/file/Footer.png"

CreateSchool__mscl = None
school_mscl = randomword(4)
NameSchool_mscl = "SMA Negeri "+school_mscl.upper()+" "+str(randint(1, 100))
CodeSchool_mscl = school_mscl.upper()

Fullname__musr = None
nama_musr = randomword(4)
FullnameMember_musr = random.choice(["Mr. ", "Mrs. ", "Miss "])+nama_musr.upper()
EmailMember_musr = nama_musr+"@mailinator.com"
HakAksesMember_musr = random.choice(["User", "Admin"])
AvatarMember_musr = ""#"/Users/wonderlabs/Desktop/damar/piquelab/image/"+random.choice(["ava1.png", "ava2.png", "ava3.png"])

NameCentre__mcntr = None
nama_mcntr = randomword(4)
NameCentre_mcntr = random.choice(["Kampus ", "Sekolah ", "Bimbingan ", "Yayasan "])+nama_mcntr.upper()
CentreAddress_mcntr = "Jalan "+nama_mcntr+" No. "+str(randint(1, 100))+" Yogyakarta, Indonesia"
AdminName_mcntr = random.choice(["Mr. ", "Mrs. ", "Miss "])+nama_mcntr.capitalize()
AdminEmail_mcntr = nama_mcntr+"@mailinator.com"

class AutomationTest(unittest.TestCase):

    SERVER = None
    EMAIL = None
    PASSWORD = None
    BROWSER = None

    @classmethod
    def setUpClass(cls):
        cls.driver = DriverBrowser(cls.BROWSER)

    def test_a_login(self):
        global LOGIN
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/login")
        self.assertEqual(self.driver.title,"Login - Tutoract")
        driver, logging, status = Login(self.driver, self.EMAIL, self.PASSWORD)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        LOGIN = status
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_a_1_create_question_mcqs(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/questions/")
            driver, logging, status = CreateQuestionMCQ(self.driver, Type1, TempTipe1, School1, Year1, Level1, Examination1, Number1)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_a_2_review_question(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/dashboard")
            driver, logging, status = ReviewQuestion(self.driver, self.EMAIL)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_a_3_create_question_oeqs(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/questions/")
            driver, logging, status = CreateQuestionOEQ(self.driver, Type2, TempTipe2, School2, Year2, Level2, Examination2, Number2)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_a_4_review_question(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/dashboard")
            driver, logging, status = ReviewQuestion(self.driver, self.EMAIL)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_b_create_exam(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/examinations/")
            driver, logging, status = CreateExam(self.driver, School_exam, Year_exam, Level_exam, Examination_exam, MCQs_exam, OEQs_exam)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_c_question_database(self):
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/databases/")
        driver, logging, status = FindQuestionDB(self.driver, Level_qb, School_qb, Year_qb, Examination_qb, QuestionType_qb)
        if "PASS" in status:
            print logging
            print status
            #pass
        else:
            self.fail(logging)
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    def test_d_sort_question(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/sort-question/")
            driver, logging, status = FindQuestion(self.driver, Level_sq, School_sq, Year_sq, Examination_sq, QuestionType_sq)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    def test_e_1_manage_templates_create(self):
        global LOGIN, Fullname_
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/templates")
            driver, logging, status = CreateTemplate(self.driver, Template_mtmp, Tipe_mtmp)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_e_2_manage_templates_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/templates")
            driver, logging, status = EditTemplate(self.driver, Template_mtmp, Tipe2_mtmp)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_e_3_manage_templates_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/templates")
            driver, logging, status = DeleteTemplate(self.driver, Template_mtmp)
            print logging, status
            self.driver.get(self.SERVER+"/templates")
            driver, logging, status = DeleteTemplate(self.driver, Template_mtmp)
            print logging, status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_f_1_manage_Classification_NewClassification(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/classifications/")
            driver, logging, status = NewClassification(self.driver, TypeClass_mclf, NameClassification_mclf, DescritionClassification_mclf, LevelClassification_mclf, ParentClassification_mclf)
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_f_2_manage_Classification_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/classifications/")
            driver, logging, status = EditClassification(self.driver, TypeClass_mclf, NameClassification_mclf)
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_f_3_manage_Classification_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/classifications/")
            driver, logging, status = DeleteClassification(self.driver, TypeClass_mclf, NameClassification_mclf+"Edited")#NameClassification+"Edited")
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_g_1_manage_tags_new(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/tags/")
            driver, logging, status = NewTags(self.driver, Tags_mtg, ChangeTags_mtg)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    def test_g_2_manage_tags_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/tags/")
            driver, logging, status = EditTags(self.driver, Tags_mtg, ChangeTags_mtg)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    def test_g_3_manage_tags_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/tags/")
            driver, logging, status = DeleteTags(self.driver, ChangeTags_mtg)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest(LOGIN)

    def test_h_create_worksheet(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/worksheets/new")
            driver, logging, status = CreateWorksheet(self.driver, WorksheetName_cwork, School_cwork, Year_cwork, Examination_cwork, Level_cwork)
            if "PASS" in status:
                print logging
                print status
                #pass
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_i_1_worksheet_database_create(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/worksheets/databases/")
            driver, logging, status = CreateFolder(self.driver, Folder)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_i_2_worksheet_database_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/worksheets/databases/")
            driver, logging, status = EditFolder(self.driver, Folder)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_i_3_worksheet_database_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/worksheets/databases/")
            driver, logging, status = DeleteFolder(self.driver, Folder+"Edited")
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_j_1_worksheet_styling_new(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = NewCover(self.driver, Name_wstly)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_j_2_worksheet_styling_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = EditCover(self.driver)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_j_3_worksheet_styling_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = DeleteCover(self.driver)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_j_4_worksheet_footer_new(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = NewFooter(self.driver, Text_Footer_wstly, Img_Footer_wstly)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_j_5_worksheet_footer_delete(self):
        global LOGIN
        if LOGIN == "Skip":#PASS
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/stylings/")
            driver, logging, status = DeleteFooter(self.driver, Text_Footer_wstly)
            if "PASS" in status:
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_k_1_manage_school_create(self):
        global LOGIN, CreateSchool_
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/schools/")
            driver, logging, status = CreateSchool(self.driver, NameSchool_mscl, CodeSchool_mscl)
            print logging
            print status
            CreateSchool_ = status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_k_2_manage_school_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/schools/")
            driver, logging, status = EditSchool(self.driver, NameSchool_mscl, CodeSchool_mscl)
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_k_3_manage_school_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/schools/")
            driver, logging, status = DeleteSchool(self.driver, NameSchool_mscl+"Edited")
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_l_1_manage_users_inviteuser(self):
        global LOGIN, Fullname__musr
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/users/")
            driver, logging, status = InviteUser(self.driver, FullnameMember_musr, EmailMember_musr, HakAksesMember_musr, AvatarMember_musr)
            print logging
            print status
            Fullname_ = status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_l_2_manage_users_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/users/")
            driver, logging, status = EditUser(self.driver, FullnameMember_musr, EmailMember_musr, HakAksesMember_musr, AvatarMember_musr)
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_l_3_manage_users_delete(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/users/")
            driver, logging, status = DeleteUser(self.driver, FullnameMember_musr+"Edited")
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_m_1_manage_centres_CreateCentre(self):
        global LOGIN, NameCentre___mcntr
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/centres/")
            driver, logging, status = CreateCentre(self.driver, NameCentre_mcntr, CentreAddress_mcntr, AdminName_mcntr, AdminEmail_mcntr)
            print logging
            print status
            NameCentre_ = status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_m_2_manage_centres_edit(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/centres/")
            driver, logging, status = EditCentre(self.driver, NameCentre_mcntr, NameCentre_mcntr+"Edited", CentreAddress_mcntr+"Edited")
            print logging
            print status
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_y_logout(self):
        global LOGIN
        if LOGIN == "PASS":
            self.startTime = time.time()
            self.driver.get(self.SERVER+"/logout")
            driver, logging, status = Logout(self.driver)
            if "PASS" in status:
                #pass
                print logging
                print status
            else:
                self.fail(logging)
            t = time.time() - self.startTime
            print "%s: %.3f" % (self.id(), t)
        else:
            self.skipTest("Login Fail")

    def test_z_forgot_password(self):
        self.startTime = time.time()
        self.driver.get(self.SERVER+"/forgot")
        driver, logging, status = ForgotPassword(self.driver, self.EMAIL)
        if "PASS" in status:
            print logging
            print status
        else:
            self.fail(logging)
        t = time.time() - self.startTime
        print "%s: %.3f" % (self.id(), t)

    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()

if __name__ == '__main__':
    command = len(sys.argv)
    if command == 5:
        AutomationTest.BROWSER = sys.argv.pop()
        AutomationTest.SERVER = sys.argv.pop()
        if "http" not in AutomationTest.SERVER:
            AutomationTest.SERVER = "http://"+AutomationTest.SERVER
        AutomationTest.PASSWORD = sys.argv.pop()
        AutomationTest.EMAIL = sys.argv.pop()
    else:
        sys.exit("ERROR : Please check again your argument")
    HTMLTestRunner.main()
